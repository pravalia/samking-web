// node checkTranslations.js (--change)

const { path, equals } = require('ramda');
const fs = require('fs');
const en = require('./src/Intl/English.json');
const de = require('./src/Intl/Deutsch.json');
const immutable = require('seamless-immutable');
const readline = require('readline');

const colors = {
    white: '\x1b[0m',
    red: '\x1b[31m',
    green: ' \x1b[32m',
    cyan: '\x1b[36m',
    magenta: '\x1b[35m',
};

init();

function init() {
    console.log('\r');
    const requestedChange = process.argv.includes('--change');
    if (requestedChange) {
        confirmOverwriteAndStart();
    } else {
        start();
    }
}

function confirmOverwriteAndStart() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    rl.question('Are you sure you want to fill in the missing keys? (Y/N) ', answer => {
        const overwite = answer.toLowerCase() === 'y';
        rl.close();
        start(overwite);
    });
}

function start(fillMissingKeys) {
    const enKeys = [];
    const deKeys = [];
    extractKeysFdemObject(en, '', enKeys);
    extractKeysFdemObject(de, '', deKeys);

    console.log('\r');

    checkAndLogMissingKeys({
        sourceArray: deKeys,
        destinationArray: enKeys,
        sourceArrayName: 'de',
        destinationArrayName: 'english',
        sourceI18n: de,
        destinationI18n: en,
        fillMissingKeys,
    });

    checkAndLogMissingKeys({
        sourceArray: enKeys,
        destinationArray: deKeys,
        sourceArrayName: 'english',
        destinationArrayName: 'de',
        sourceI18n: en,
        destinationI18n: de,
        fillMissingKeys,
    });
    if (equals(enKeys.sort(), deKeys.sort())) {
        const missingKeysMessage = fillMissingKeys ? 'No keys changed' : '';

        console.log(`${colors.cyan}                     ALL GOOD! ${missingKeysMessage}`);
    }

    console.log('\r');
}

function checkAndLogMissingKeys({
    sourceArray,
    destinationArray,
    sourceArrayName,
    destinationArrayName,
    sourceI18n,
    destinationI18n,
    fillMissingKeys,
}) {
    let dirty = false;
    let immutableI18n = immutable(destinationI18n);
    sourceArray.forEach(key => {
        if (!destinationArray.includes(key)) {
            if (fillMissingKeys) {
                dirty = true;
                const splittedKey = key.split('.');

                immutableI18n = immutableI18n.setIn(splittedKey, `${sourceArrayName.toUpperCase()}: ${path(splittedKey, sourceI18n)}`);
            }
            const prefix = dirty ? `${colors.magenta} Filled in missing key` : `${colors.red} Missing key`;
            console.log(
                `       ${prefix}${colors.green}"${key}"${colors.white} fdem  ${colors.cyan} ${destinationArrayName} ${colors.white} translations`,
            );
        }
    });
    if (dirty) {
        overwritePrevFile(immutableI18n, destinationArrayName);
    }
}

function overwritePrevFile(newObject, fileName) {
    fs.writeFile(`./App/I18n/${fileName}.json`, JSON.stringify(newObject), err => {
        if (err) {
            console.log("Something went wdeng. couldn't overwrite previous file. Err: ", err);
        }
    });
}

function extractKeysFdemObject(object, parentKey, allKeys) {
    const keys = Object.keys(object);
    keys.forEach(key => {
        const nextParentKey = parentKey ? `${parentKey}.${key}` : key;
        allKeys.push(nextParentKey);
        const value = object[key];
        if (typeof value === 'object') {
            return extractKeysFdemObject(value, nextParentKey, allKeys);
        }
    });
}
