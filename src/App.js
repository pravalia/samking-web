import React, { Component } from 'react';
import './App.css';

import { addLocaleData, defineMessages, IntlProvider } from 'react-intl';
import Router from './Router';
import en from 'react-intl/locale-data/en';
import de from 'react-intl/locale-data/de';
import { connect } from 'react-redux';
import { Provider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import { Metrics } from './Utils';

addLocaleData([...en, ...de]);

Array.prototype.includes =
    Array.prototype.includes ||
    function(string) {
        return this.indexOf(string) > -1;
    };

class App extends Component {
    constructor(props) {
        super(props);
        const { user } = props;
        this.state = {
            messages: this.getNewMessages((user && user.language) || 'Deutsch'),
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.language !== this.props.language) {
            this.setState({
                messages: this.getNewMessages(nextProps.user.language),
            });
        }
    }

    getNewMessages(language = 'Deutsch') {
        const selectedLanguage = language;
        let localizedMessages = require('./Intl/Deutsch.json');
        try {
            localizedMessages = require(`./Intl/${selectedLanguage}.json`);
        } catch (e) {}
        return defineMessages(localizedMessages);
    }

    render() {
        const alertOptions = {
            timeout: 5000,
            position: 'bottom center',
        };

        return Metrics.isShitBrowser ? (
            <IntlProvider locale={'en'} messages={this.state.messages}>
                <Router />
            </IntlProvider>
        ) : (
            <IntlProvider locale={'en'} messages={this.state.messages}>
                <Provider template={AlertTemplate} {...alertOptions}>
                    <Router />
                </Provider>
            </IntlProvider>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
    };
};

export default connect(
    mapStateToProps,
    null,
)(App);
