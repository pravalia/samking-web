import React from 'react';
import { AppStyles, Metrics } from './Utils';
import SignInForm from './Components/Forms/SignInForm';
import { connect } from 'react-redux';
import UserActions from './Redux/User/UserRedux';
import Spinner from './Components/Spinner';
import Home from './Containers/Home';
import MerchantsContainer from './Containers/MerchantsContainer';
import ClientHistory from './Containers/ClientHistory';
import AppActions from './Redux/AppRedux';
import AdsContainer from './Containers/AdsContainer';
import EditProfileContainer from './Containers/EditProfileContainer';
import Navbar from './Containers/Navbar';
import SignUpContainer from './Containers/SignUpContainer';
import { withAlert } from 'react-alert';
import { injectIntl } from 'react-intl';
import HistoryActions from './Redux/History/HistoryRedux';
import ChangePasswordContainer from './Containers/ChangePasswordContainer';
import ClientDetails from './Components/ClientDetails';
import { equals } from 'ramda';
import { HashRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import ForgotPasswordContainer from './Containers/ForgotPasswordContainer';
import ResetPasswordContainer from './Containers/ResetPasswordContainer';

const NO_ADS_SCREENS = ['#/login', '#/signup'];
class MainRouter extends React.Component {
    state = {
        signedIn: false,
        signedOut: false,
        numberOfNewPoints: 0,
    };

    componentDidMount() {
        this.props.getLanguages();
        this.props.getCountries();
        window.onhashchange = this.onHashChange;
    }

    onHashChange = () => {
        this.props.getAdsRequest();
        this.forceUpdate();
    };

    componentWillReceiveProps(nextProps) {
        const { user, history, availableOffer } = nextProps;
        const hasAvailableOffer = Boolean(availableOffer && !equals(this.props.availableOffer, availableOffer));
        if (hasAvailableOffer) {
            return this.handleAvailableOffer(availableOffer);
        }

        if ((!user || !user.id) && this.props.user.id && this.notificationsWorker) {
            this.notificationsWorker = null;
        }

        if (window.Worker && user && history && history.ranking && (!this.notificationsWorker || user.id !== this.props.user.id)) {
            this.notificationsWorker = new Worker('./worker.js');
            this.notificationsWorker.postMessage({
                points: history.ranking.NumberOfPoints,
                apiKey: user.token.token,
            });
            this.notificationsWorker.onmessage = message => {
                this.setState({
                    numberOfNewPoints: message.data.numberOfNewPoints,
                    received: message.data.received,
                });
            };
        } else if (!window.Worker) {
            console.log('no worker suppported');
        }
        const { id } = nextProps.user;
        const { id: prevId } = this.props.user;
        const signedIn = id && !prevId;
        const signedOut = !id;

        this.setState({
            signedIn,
            signedOut,
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { alert, availableOffer } = this.props;
        const { numberOfNewPoints: prevPoints } = prevState;
        const { numberOfNewPoints, received } = this.state;
        const receivedPoints = !prevPoints && numberOfNewPoints && numberOfNewPoints > 0;

        if (receivedPoints) {
            return this.handleReceivedPoints();
        }
    }

    handleAvailableOffer = availableOffer => {
        const { intl, alert } = this.props;
        if (!availableOffer) {
            return;
        }
        alert &&
            alert.success &&
            alert.success(availableOffer.message, {
                timeout: 0,
                onClose: () => this.props.removeAvailableOffer(availableOffer.id),
            });
    };

    handleReceivedPoints = () => {
        const { intl, alert } = this.props;
        const { messages } = intl;
        const { received, numberOfNewPoints } = this.state;
        const message1 = received ? messages.receivedNewPoints1 : messages.spentPoints1;
        const message2 = received ? messages.receivedNewPoints2 : messages.spentPoints2;

        alert && alert.success && alert.success(`${message1} ${numberOfNewPoints} ${message2}`, { timeout: 0 });
        this.props.getHistoryRequest();
        this.setState({
            numberOfNewPoints: 0,
        });
    };

    getRedirects = () => {
        const { signedIn, signedOut } = this.state;
        const { pathname } = window.location;

        return {
            signedOutRedirect: signedOut && pathname !== '#/login' ? <Redirect to="/login" /> : null,
            signedInRedirect: signedIn && pathname !== '#/home' ? <Redirect from="/login" to="/home" /> : null,
        };
    };

    render() {
        const { signedInRedirect, signedOutRedirect } = this.getRedirects();
        const onlyOneAdd = window.location.hash === '#/merchants';
        const noAds = NO_ADS_SCREENS.includes(window.location.hash);
        return (
            <React.Fragment>
                <div
                    style={{
                        ...AppStyles.controls.appView,
                        marginBottom: noAds ? '0px' : onlyOneAdd ? '-60px' : '-123px',
                        paddingBottom: noAds ? '0px' : onlyOneAdd ? '60px' : '123px',
                        justifyContent: 'flex-start',
                        minHeight: Metrics.screenHeight,
                    }}
                >
                    <Spinner />

                    <Router>
                        <React.Fragment>
                            <Navbar />
                            <ClientDetails />

                            <Switch>
                                <Route path="/signup" component={SignUpContainer} />
                                <Route path="/forgotpassword" component={ForgotPasswordContainer} />
                                <Route path="/resetpassword" component={ResetPasswordContainer} />
                                {signedInRedirect}
                                <Route path="/login" component={SignInForm} />
                                {signedOutRedirect}
                                <Route path="/merchants" component={MerchantsContainer} />
                                <Route path="/history" component={ClientHistory} />
                                <Route path="/profile" component={EditProfileContainer} />
                                <Route path="/changePassword" component={ChangePasswordContainer} />
                                <Route path="/home" component={Home} />
                                <Redirect from="/" to="/login" />
                            </Switch>
                        </React.Fragment>
                    </Router>
                </div>
                <AdsContainer />
		<AdsContainer secondary />
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    const history = state.history || {};
    return {
        user: state.user,
        history,
        availableOffer: history.availableOffer,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setLanguage: language => dispatch(UserActions.setLanguage(language)),
        getHistoryRequest: () => dispatch(HistoryActions.getHistoryRequest()),
        removeAvailableOffer: id => dispatch(HistoryActions.removeAvailableOffer(id)),
        getLanguages: () => dispatch(UserActions.getLanguagesRequest()),
        getCountries: () => dispatch(UserActions.getCountriesRequest()),
        getAdsRequest: () => dispatch(AppActions.getAdsRequest()),
    };
};

export default injectIntl(
    withAlert(
        connect(
            mapStateToProps,
            mapDispatchToProps,
        )(MainRouter),
    ),
);
