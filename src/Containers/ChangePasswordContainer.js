import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import PasswordActions from '../Redux/User/PasswordRedux';
import { Metrics, Colors } from '../Utils';
import { Form } from 'react-form';
import { withAlert } from 'react-alert';
import { Redirect } from 'react-router-dom';
import { Button, FormGroup, ControlLabel, FormControl, Checkbox } from 'react-bootstrap';

class ChangePasswordContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hidePasswords: true,
            fields: props.passwordForm.fields,
        };
    }

    componentWillReceiveProps(nextProps) {
        const { success, errorMessage } = nextProps.passwordForm;
        if (success && !this.props.passwordForm.success) {
            this.props.alert.success(this.props.intl.messages.changePasswordSuccess, {
                onClose: () => this.setState({ goBack: true }),
            });
            this.props.resetPasswordForm();
        }
        this.setState({
            fields: nextProps.passwordForm.fields,
        });
        if (errorMessage && !this.props.errorMessage) {
            this.props.alert.error(errorMessage);
            this.props.resetPasswordFormFlags();
        }
    }

    checkIfPasswordsMatch = () => {
        const { newPassword, repeatPassword } = this.state.fields;
        const { intl } = this.props;
        const passwordsMatch = newPassword === repeatPassword;
        if (!passwordsMatch) {
            this.setState(prevState => {
                return {
                    fields: {
                        ...prevState.fields,
                        newPasswordError: intl.messages.passwordsDontMatch,
                        repeatPasswordError: intl.messages.passwordsDontMatch,
                    },
                };
            });
        }
        return passwordsMatch;
    };

    onChange = (field, value) => {
        this.props.onPasswordFormFieldChange(field, value);
    };

    onToggleHidePasswords = () => {
        this.setState(prevState => {
            return {
                hidePasswords: !prevState.hidePasswords,
            };
        });
    };

    onSubmit = () => {
        this.props.checkforEmptyFields(this.props.intl);
        if (this.props.passwordForm.isValid) {
            if (this.checkIfPasswordsMatch()) {
                this.props.updatePasswordRequest();
            }
        }
    };

    goBack = () => {
        this.setState({ goBack: true });
    };

    render() {
        const { intl, passwordForm } = this.props;
        const { oldPassword, newPassword, repeatPassword, oldPasswordError, newPasswordError, repeatPasswordError } = this.state.fields;
        if (this.state.goBack) {
            return <Redirect to="/profile" />;
        }
        const matches = window.innerWidth < 720;

        return (
            <div style={styles.formContainer}>
                <p style={styles.title}>{this.props.intl.messages.changePasswordCTA}</p>

                <div
                    style={{
                        marginTop: matches ? '0px' : '30px',
                        display: 'flex',
                        justifyContent: 'center',
                    }}
                >
                    <Form onSubmit={this.onSubmit}>
                        <form
                            style={{
                                ...styles.form,
                                padding: matches ? '15px 0' : '0px',
                            }}
                            id="updatePassword"
                        >
                            <div style={{ marginTop: '5px' }}>
                                <FormGroup controlId="oldPasswordGroup" validationState={oldPasswordError ? 'error' : null}>
                                    <ControlLabel>{oldPasswordError ? oldPasswordError : intl.messages.oldPassword}</ControlLabel>
                                    <FormControl
                                        type={this.state.hidePasswords ? 'password' : 'text'}
                                        value={oldPassword}
                                        onChange={e => this.onChange('oldPassword', e.target.value)}
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup controlId="newPasswordGroup" validationState={newPasswordError ? 'error' : null}>
                                    <ControlLabel>{newPasswordError ? newPasswordError : intl.messages.newPassword}</ControlLabel>
                                    <FormControl
                                        type={this.state.hidePasswords ? 'password' : 'text'}
                                        value={newPassword}
                                        onChange={e => this.onChange('newPassword', e.target.value)}
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>{' '}
                                <FormGroup controlId="repeatPasswordGroup" validationState={repeatPasswordError ? 'error' : null}>
                                    <ControlLabel>{repeatPasswordError ? repeatPasswordError : intl.messages.repeatPassword}</ControlLabel>
                                    <FormControl
                                        type={this.state.hidePasswords ? 'password' : 'text'}
                                        value={repeatPassword}
                                        onChange={e => this.onChange('repeatPassword', e.target.value)}
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup controlId="hidePasswords">
                                    <Checkbox
                                        checked={this.state.hidePasswords}
                                        onChange={this.onToggleHidePasswords}
                                        className="signInCheckbox"
                                        style={styles.checkbox}
                                    >
                                        {intl.messages.hidePasswords}
                                    </Checkbox>
                                </FormGroup>
                                {passwordForm.errorMessage && (
                                    <span
                                        style={{
                                            ...styles.formRow,
                                            ...styles.error,
                                        }}
                                    >
                                        {passwordForm.errorMessage}
                                    </span>
                                )}
                                <div style={styles.formRow}>
                                    <Button
                                        bsSize="lg"
                                        bsStyle="success"
                                        onClick={this.onSubmit}
                                        type="button"
                                        style={{
                                            width: matches ? '250px' : '300px',
                                            backgroundColor: Colors.appColor,
                                        }}
                                    >
                                        {intl.messages.changePasswordCTA}
                                    </Button>
                                </div>
                                <div style={styles.formRow}>
                                    <Button
                                        style={{
                                            width: matches ? '250px' : '300px',
                                            marginTop: '1em',
                                            backgroundColor: Colors.appSecondaryColor,
                                        }}
                                        onClick={this.goBack}
                                    >
                                        {intl.messages.backToEditProfile}
                                    </Button>
                                </div>
                            </div>
                        </form>
                    </Form>
                </div>
            </div>
        );
    }
}

const styles = {
    formWrapper: {
        display: 'flex',
        flexGrow: 1,
        marginTop: Metrics.baseMargin,
    },
    formContainer: {
        flexGrow: 1,
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '10px',
    },
    bottomCTAWrapper: {
        display: 'flex',

        justifyContent: 'center',
        alignContent: 'center',
    },
    checkbox: {
        display: 'flex',

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
};

const mapStateToProps = state => {
    return {
        passwordForm: state.passwordForm || { fields: {} },
    };
};
const mapDispatchToProps = dispatch => {
    return {
        updatePasswordRequest: () => dispatch(PasswordActions.updatePasswordRequest()),
        onPasswordFormFieldChange: (field, value) => dispatch(PasswordActions.onPasswordFormFieldChange(field, value)),
        resetPasswordForm: () => dispatch(PasswordActions.resetPasswordForm()),
        resetPasswordFormFlags: () => dispatch(PasswordActions.resetPasswordFormFlags()),
        checkforEmptyFields: intl => dispatch(PasswordActions.checkForEmptyFields(intl)),
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(withAlert(ChangePasswordContainer)),
);
