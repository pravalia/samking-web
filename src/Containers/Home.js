/*global QRCode*/
import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { Button } from 'react-bootstrap';
import { AppStyles, Metrics } from '../Utils';
import { Redirect } from 'react-router-dom';

class Home extends React.Component {
    qrWidth = 0;

    calculateWidth = () => {
        const qrWidth = Metrics.screenHeight - 48 - 200 - 43 - 10 - 16 * 3;
        this.qrWidth = qrWidth > 250 ? 250 : qrWidth;
    };

    downloadQr = () => {
        const canvas = document.getElementById('qrCode');
        const button = document.getElementById('testHref');
        button.href = canvas.children[0].toDataURL('image/jpg');
        button.click();
    };

    componentWillMount() {
        this.calculateWidth();
    }

    componentDidMount() {
        window.addEventListener('onresize', this.calculateWidth);
        const elem = document.getElementById('qrCode');

        elem &&
            new QRCode(elem, {
                text: this.props.user.apiKey || '',
                width: this.qrWidth,
                height: this.qrWidth,
                colorDark: '#000000',
                colorLight: '#ffffff',
                correctLevel: QRCode.CorrectLevel.H,
            });
    }

    componentWillUnmount() {
        window.removeEventListener('onresize', this.calculateWidth);
    }

    render() {
        const { user } = this.props;
        if (!this.props.user || !this.props.user.id) {
            return <Redirect to="/login" />;
        }

        return (
            <div style={AppStyles.screen.appView}>
                <div style={styles.qrCodeWrapper}>
                    <a style={{ display: 'none' }} href="" id="testHref" download={`Samking Code - ${user.firstName} ${user.lastName}.png`} />
                    <div
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            flexFlow: 'column',
                            marginLeft: Metrics.isShitBrowser ? '43%' : '0',
                        }}
                    >
                        <div
                            style={{
                                width: `${this.qrWidth}px`,
                                height: `${this.qrWidth}px`,
                            }}
                            id={'qrCode'}
                        />
                        <Button className="qrDownloadBtn" id="downloadButton" bsStyle="primary" style={styles.button} onClick={this.downloadQr}>
                            Download Qr Code
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

const styles = {
    qrCodeWrapper: {
        marginTop: Metrics.isIE ? '5em' : 0,
        display: 'flex',
        flex: '1',
        justifyContent: 'center',
        alignItems: 'center',
        flexFlow: 'column',
    },
    button: {
        marginTop: window.innerWidth < 720 ? '10px' : '20px',
        minHeight: '2.5em',
        marginLeft: Metrics.isShitBrowser ? '2.5em' : 0,
        backgroundColor: '#d2aa6d',
        border: 'none',
        fontSize: '17px',
    },
};

const mapStateToProps = state => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {};
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Home),
);
