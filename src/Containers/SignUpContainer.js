import React from 'react';
import { Form } from 'react-form';
import { connect } from 'react-redux';
import UserActions from '../Redux/User/UserRedux';
import { Metrics, Colors } from '../Utils';
import { injectIntl } from 'react-intl';
import { Redirect } from 'react-router-dom';
import { withAlert } from 'react-alert';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import { GridLoader } from 'react-spinners';
const config = require('../config');

class SignUpContainer extends React.Component {
    constructor(props) {
        super(props);
        const { search, searchParam } = this.getSearchParams();
        const defaultLanguageId =
            search === 'l' ? this.getDefaultLanguage(searchParam) : 1;
        this.state = {
            dropdownOpen: false,
            goBack: false,
            fields: {
                firstName: '',
                lastName: '',
                displayName: '',
                email: '',
                password: '',
                repeatPassword: '',
                countryId: '',
                city: '',
                zipCode: '',
                phone: '',
                languageId:
                    props.user.id > 0 && props.user.languageId != null
                        ? props.user.languageId
                        : defaultLanguageId,
                firstNameError: '',
                lastNameError: '',
                displayNameError: '',
                emailError: '',
                phoneError: '',
                cityError: '',
                countryIdError: '',
                zipCodeError: '',
                passwordError: '',
                repeatPasswordError: ''
            }
        };
    }

    componentDidMount() {
        this.props.resetUser();
        const { searchParam } = this.getSearchParams();
        const normalizedParam = String(searchParam).toLowerCase();
        const newMessage = normalizedParam === 'de' ? 'Deutsch' : 'English';
        this.props.setUserLanguage(newMessage);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.id && !this.props.user.id) {
            this.props.alert.success(this.props.intl.messages.signUpSuccess);
        }
    }

    getDefaultLanguage = queryString => {
        return String(queryString).toLowerCase() === 'de' ? 1 : 2;
    };

    onSubmit = e => {
        this.props.signUpRequest({
            ...this.state.fields
        });
    };

    onClick = () => {
        const {
            password,
            repeatPassword,
            email,
            firstName,
            lastName,
            displayName,
            zipCode,
            city
        } = this.state.fields;
        if (
            password &&
            email &&
            firstName &&
            lastName &&
            displayName &&
            repeatPassword &&
            city &&
            zipCode &&
            this.isValidCountry()
        ) {
            if (this.passwordsMatch()) {
                this.onSubmit();
            }
        } else {
            this.setErrors();
        }
    };

    getSearchParams = () => {
        const { hash } = window.location;
        const queryString = hash.substr(hash.indexOf('?') + 1);
        const search = queryString.substr(0, queryString.indexOf('='));
        const searchParam = queryString.substr(queryString.indexOf('=') + 1);
        return { search, searchParam };
    };

    passwordsMatch = () => {
        const { password, repeatPassword } = this.state.fields;
        const match = password === repeatPassword;
        if (!match) {
            this.setState(prevState => {
                return {
                    fields: {
                        ...prevState.fields,
                        passwordError: this.props.intl.messages
                            .passwordsDontMatch,
                        repeatPasswordError: this.props.intl.messages
                            .passwordsDontMatch
                    }
                };
            });
        }
        return match;
    };

    setErrors = () => {
        const errors = {};
        Object.keys(this.state.fields).forEach(key => {
            if (key === 'countryId' && !this.isValidCountry()) {
                errors[`${key}Error`] = `${
                    this.props.intl.messages.selectValidCountry
                }`;
                return;
            }
            if (!this.state.fields[key] && !key.includes('Error')) {
                errors[`${key}Error`] = `${
                    this.props.intl.messages.cannotBeBlank
                }`;
            }
        });
        this.setState({
            fields: {
                ...this.state.fields,
                ...errors
            }
        });
    };

    isValidCountry = () => {
        return (
            this.props.user.countries.find(
                ({ id }) => id === this.state.fields.countryId
            ) != null
        );
    };

    onChange = (field, value) => {
        if (this.props.user.errorMessage) {
            this.props.resetUserFlags();
        }
        this.setState(prevState => {
            return {
                fields: {
                    ...prevState.fields,
                    [field]: value,
                    firstNameError: '',
                    lastNameError: '',
                    passwordError: '',
                    repeatPasswordError: '',
                    displayNameError: '',
                    emailError: '',
                    phoneError: '',
                    countryIdError: '',
                    cityError: '',
                    zipCodeError: ''
                }
            };
        });
    };

    onSelectCountry = country => {
        if (country == null) return;
        this.onChange('countryId', Number(country.value));
    };

    onLanguageChange = language => {
        this.setState({
            dropdownOpen: false
        });
        if (language == null) return;
        this.onChange('languageId', Number(language.value));
        this.props.setUserLanguage(language.label);
    };

    getLanguages = () => {
        return this.props.user.languages.map(({ name, id }) => ({
            value: id,
            label: name
        }));
    };

    onToggleDropdown = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    };

    goBack = () => {
        this.setState({
            goBack: true
        });
    };

    getCountries = () => {
        return this.props.user.countries.map(({ name, id }) => ({
            value: id,
            label: name
        }));
    };

    getTermsAndConditions = () => {
        const { intl } = this.props;
        const {
            tc1,
            tc2,
            tcLink,
            cookies1,
            cookies2,
            cookiesLink1,
            cookiesLink2
        } = intl.messages;
        const matches = window.innerWidth < 720;
        return (
            <div
                style={{
                    flexDirection: 'column',
                    display: 'flex',
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    width: matches ? '275px' : 'auto',
                    paddingLeft: '25px',
                    alignSelf: 'flex-start'
                }}
            >
                <div style={styles.tcRow}>
                    <p style={styles.tc}>{tc1}</p>
                    <a
                        target="_blank"
                        href={config.tcLink}
                        style={styles.tcLink}
                    >
                        {tcLink}
                    </a>
                    <p style={styles.tc}>{tc2}</p>
                </div>
                <div
                    style={{ ...styles.tcRow, marginTop: matches ? '5px' : 0 }}
                >
                    <p style={styles.tc}>{cookies1}</p>
                    <a
                        target="_blank"
                        style={styles.tcLink}
                        href={config.privacyPolicyLink}
                    >
                        {cookiesLink1}
                    </a>
                    <p style={styles.tc}>{cookies2}</p>
                    <a
                        target="_blank"
                        style={styles.tcLink}
                        href={config.cookiesLink}
                    >
                        {cookiesLink2}
                    </a>
                </div>
            </div>
        );
    };

    render() {
        const { intl, user } = this.props;
        const { fields } = this.state;
        const {
            passwordError,
            repeatPasswordError,
            emailError,
            firstNameError,
            lastNameError,
            displayNameError,
            countryIdError,
            countryId,
            cityError,
            city,
            zipCode,
            zipCodeError
        } = fields;
        const matches = window.innerWidth < 720;
        if (this.props.user.id) {
            return <Redirect to="/home" />;
        }
        if (this.state.goBack) {
            return <Redirect to="/login" />;
        }
        if (!this.props.user.languages) {
            return <GridLoader loading={true} color={Colors.appColor} />;
        }

        const countries = this.getCountries();
        const languages = this.getLanguages();
        return (
            <div
                style={{
                    marginTop: matches ? '30px' : '70px',
                    display: matches ? 'initial' : 'flex',
                    justifyContent: 'center'
                }}
            >
                <Form onSubmit={this.onSubmit}>
                    <form
                        style={{
                            ...styles.form,
                            display: matches ? 'initial' : 'flex',
                            flexDirection: matches ? 'row' : 'column',
                            justifyContent: 'center'
                        }}
                        id="updateForm"
                    >
                        <p
                            style={{
                                ...styles.header,
                                marginTop: matches ? '-30px' : '0px'
                            }}
                        >
                            {' '}
                            {intl.messages.signUp}{' '}
                        </p>

                        <div
                            style={{
                                marginTop: 0,
                                display: 'flex',
                                flexDirection: matches ? 'column' : 'row',
                                width: matches ? '300px' : '800px',
                                justifyContent: 'space-around',
                                alignItems: 'center'
                            }}
                        >
                            <div
                                style={{
                                    width: matches ? '250px' : '350px'
                                }}
                            >
                                <FormGroup
                                    controlId="firstNameGroup"
                                    validationState={
                                        firstNameError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {firstNameError
                                            ? `${
                                                  intl.messages.firstName
                                              }: ${firstNameError}`
                                            : intl.messages.firstName + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'firstName',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="lastNameGroup"
                                    validationState={
                                        lastNameError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {lastNameError
                                            ? `${
                                                  intl.messages.lastName
                                              }: ${lastNameError}`
                                            : intl.messages.lastName + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'lastName',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="displayNameGroup"
                                    validationState={
                                        displayNameError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {displayNameError
                                            ? `${
                                                  intl.messages.displayName
                                              }: ${displayNameError}`
                                            : intl.messages.displayName + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'displayName',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="emailGroup"
                                    validationState={
                                        emailError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {emailError
                                            ? `${
                                                  intl.messages.email
                                              }: ${emailError}`
                                            : intl.messages.email + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'email',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="phoneGroup"
                                    validationState={null}
                                >
                                    <ControlLabel>
                                        {intl.messages.phone}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'phone',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                            </div>
                            <div
                                style={{
                                    width: matches ? '250px' : '350px'
                                }}
                            >
                                <p
                                    style={{
                                        fontWeight: 'bold',
                                        color: countryIdError
                                            ? 'rgb(169, 68, 66)'
                                            : 'black',
                                        marginBottom: '5px'
                                    }}
                                >
                                    {countryIdError
                                        ? `${countryIdError}`
                                        : intl.messages.country + ' *'}
                                </p>
                                <Select
                                    name="countries"
                                    value={countryId}
                                    onChange={this.onSelectCountry}
                                    options={countries}
                                    onClose={() => null}
                                    style={{ marginBottom: '15px' }}
                                />
                                <FormGroup
                                    controlId="cityGroup"
                                    validationState={cityError ? 'error' : null}
                                >
                                    <ControlLabel>
                                        {cityError
                                            ? `${
                                                  intl.messages.city
                                              }: ${cityError}`
                                            : intl.messages.city + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={city}
                                        onChange={e =>
                                            this.onChange(
                                                'city',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>

                                <FormGroup
                                    controlId="zipCodeGroup"
                                    validationState={
                                        zipCodeError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {zipCodeError
                                            ? `${
                                                  intl.messages.zipCode
                                              }: ${zipCodeError}`
                                            : intl.messages.zipCode + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={zipCode}
                                        onChange={e =>
                                            this.onChange(
                                                'zipCode',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="passwordGroup"
                                    validationState={
                                        passwordError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {passwordError
                                            ? `${
                                                  intl.messages.password
                                              }: ${passwordError}`
                                            : intl.messages.password + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="password"
                                        onChange={e =>
                                            this.onChange(
                                                'password',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="repeatPasswordGroup"
                                    validationState={
                                        repeatPasswordError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {repeatPasswordError
                                            ? `${
                                                  intl.messages.repeatPassword
                                              }: ${repeatPasswordError}`
                                            : intl.messages.repeatPassword +
                                              ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="password"
                                        onChange={e =>
                                            this.onChange(
                                                'repeatPassword',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                            </div>
                        </div>
                        {this.getTermsAndConditions()}

                        <div
                            style={{
                                ...styles.formRow,
                                flexDirection: 'column',
                                display: 'flex'
                            }}
                        >
                            {user.errorMessage && (
                                <span
                                    style={{
                                        ...styles.formRow,
                                        ...styles.error,
                                        padding: '0.5em'
                                    }}
                                >
                                    {user.errorMessage}
                                </span>
                            )}
                            <div style={{ marginTop: '10px' }}>
                                <p
                                    style={{
                                        fontWeight: 'bold',
                                        alignSelf: 'flex-start'
                                    }}
                                >
                                    {intl.messages.language}
                                </p>
                                <Select
                                    name="language"
                                    value={this.state.fields.languageId}
                                    onChange={this.onLanguageChange}
                                    options={languages}
                                    style={{
                                        marginBottom: '16px',
                                        width: matches ? '250px' : '300px'
                                    }}
                                />
                            </div>
                            <Button
                                bsSize="lg"
                                bsStyle="primary"
                                onClick={() => this.onClick()}
                                type="button"
                                style={{
                                    width: matches ? '250px' : '300px',
                                    backgroundColor: Colors.appColor,
                                    marginBottom: '16px'
                                }}
                            >
                                {intl.messages.signUp}
                            </Button>
                            <Button
                                onClick={this.goBack}
                                type="button"
                                style={{
                                    width: matches ? '250px' : '300px',
                                    backgroundColor: Colors.appSecondaryColor
                                }}
                            >
                                {intl.messages.backToLogin}
                            </Button>
                        </div>
                    </form>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        signUpRequest: payload => dispatch(UserActions.signUpRequest(payload)),
        resetUser: () => dispatch(UserActions.resetUser()),
        resetUserFlags: () => dispatch(UserActions.resetUserFlags()),
        setUserLanguage: language => dispatch(UserActions.setLanguage(language))
    };
};

const styles = {
    tcRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        flexWrap: 'wrap',
        wordWrap: 'wrap'
    },
    header: {
        fontSize: '18px',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    tc: {
        margin: 0,
        fontSize: '14px',
        flexWrap: 'wrap',
        paddingRight: '5px'
    },
    tcLink: {
        paddingRight: '5px',
        fontSize: '14px',
        flexWrap: 'wrap'
    },
    form: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        display: 'flex',
        maxWidth: window.innerWidth,
        border: '2px dotted rgba(0,0,0,0.2)',
        marginTop: '5px',
        padding: '10px'
    },
    formRow: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        display: 'flex',
        marginBottom: '1em'
    },
    formLabel: {
        display: 'flex',
        alignSelf: 'flex-start',
        marginLeft: '1.9em',
        marginBottom: '.3em'
    },
    checkboxWrapper: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    checkbox: {
        fontSize: '1.1em',
        fontWeight: 'bold'
    },
    inputField: {
        padding: '.5em .5em'
    },
    formError: {
        color: '#cc0000'
    },
    error: {
        backgroundColor: '#cc0000',
        color: 'white',
        paddingTop: '.5em',
        paddingBottom: '.5em'
    },
    button: {
        alignSelf: 'center'
    }
};

const isMobileStyles = {
    true: {
        form: {
            width: '22em'
        },
        formRow: {
            width: '20em'
        }
    },
    false: {
        form: {
            width: '30em'
        },
        formRow: {
            width: '300px'
        }
    }
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withAlert(SignUpContainer))
);
