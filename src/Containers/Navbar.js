import React from 'react';
import { Metrics, Colors } from '../Utils';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import UserActions from '../Redux/User/UserRedux';
import { Button } from 'react-bootstrap';

class Navbar extends React.Component {
    state = {
        isMenuOpen: false,
        linksCalculated: 0,
    };

    componentDidMount() {
        this.incrementLinksTotalWidth();
    }

    signOut = () => {
        this.toggleMobileMenu();
        this.props.signOut();
    };

    getNavbarIcons = () => {
        return (
            <React.Fragment>
                {' '}
                <Link id="linkToProfile" className="navLink" to={'/profile'} style={styles.route}>
                    {this.props.intl.messages.routes.editProfile}
                </Link>
                <Button
                    id="linkToSignOut"
                    onClick={this.props.signOut}
                    type="button"
                    className="navLink"
                    to={'/signOut'}
                    style={{ ...styles.route, ...styles.signOut }}
                >
                    {this.props.intl.messages.routes.signOut}
                </Button>
            </React.Fragment>
        );
    };

    toggleMobileMenu = () => {
        const { isMenuOpen } = this.state;
        const drawer = document.getElementById('drawer');
        var drawerAnimation = [
            {
                right: isMenuOpen ? '0px' : '-100px',
            },
            {
                right: isMenuOpen ? '-100px' : '0px',
            },
        ];

        const options = { duration: 300, fill: 'forwards' };
        drawer.animate ? drawer.animate(drawerAnimation, options) : (drawer.style.right = isMenuOpen ? '-100px' : '0px');

        const timing = Number(isMenuOpen) * 300;
        setTimeout(
            () =>
                this.setState(prevState => {
                    return {
                        isMenuOpen: !prevState.isMenuOpen,
                    };
                }),
            timing,
        );
    };

    incrementLinksTotalWidth = e => {
        const linksNames = ['linkToProfile', 'linkToHome', 'linkToHistory', 'linkToMerchants', 'linkToSignOut'];
        const width = linksNames.reduce((width, linkName, idx) => {
            const element = document.getElementById(linkName);
            if (!element) return width;
            return width + element.getBoundingClientRect().width;
        }, 0);
        this.setState({
            linksWidth: width + 250,
        });
    };

    getMobileRightMenu = () => {
        return (
            <div
                id="drawer"
                style={{
                    ...styles.menuRightWrapper,
                    display: this.state.isMenuOpen ? 'flex' : ' none',
                }}
            >
                <Link id="linkToSignOut" className="navLink" to={'/profile'} style={{ ...styles.route }} onClick={this.toggleMobileMenu}>
                    {this.props.intl.messages.routes.editProfile}
                </Link>
                <a onClick={this.signOut} className="navLink" style={{ ...styles.route, cursor: 'pointer' }}>
                    {this.props.intl.messages.routes.signOut}
                </a>
            </div>
        );
    };

    render() {
        if (!this.props.user.id) {
            return null;
        }
        const comparisonWidth = this.state.linksWidth ? this.state.linksWidth : 400;
        const { intl } = this.props;
        const hasRightMenu = window.innerWidth < 450 || window.innerWidth < comparisonWidth;

        const merchantsLink =
            'geolocation' in navigator ? (
                <Link id="linkToMerchants" className="navLink" to="/merchants" style={styles.route}>
                    {intl.messages.routes.merchants}
                </Link>
            ) : null;
        const rightPartOfNavbar = hasRightMenu ? this.getMobileRightMenu() : this.getNavbarIcons();

        const mobileMenuIcon = hasRightMenu && (
            <a style={styles.mobileMenuTrigger} onClick={this.toggleMobileMenu}>
                <img alt={'menu'} src={require('../Images/mobile-menu.png')} style={styles.mobileMenuTrigger} />
            </a>
        );

        return (
            <div
                id="wrapper"
                style={{
                    ...styles.routesWrapper,
                    justifyContent: 'center',
                }}
            >
                <img alt={'logo'} src={require('../Images/samkinglogo.ico')} style={styles.logo} />
                <Link id="linkToHome" className="navLink" to={'/home'} style={styles.route}>
                    {intl.messages.routes.home}
                </Link>
                {merchantsLink}
                <Link id="linkToHistory" className="navLink" to={'/history'} style={styles.route}>
                    {intl.messages.routes.history}
                </Link>
                {rightPartOfNavbar}
                {mobileMenuIcon}
            </div>
        );
    }
}

const styles = {
    header: {
        margin: '0px',
        color: 'white',
    },
    mobileMenuTrigger: {
        width: '38',
        height: '38px',
        position: 'absolute',
        right: '10px',
        top: '3px',
    },
    logo: {
        height: window.innerWidth < 720 ? '40px' : '50px',
        position: 'absolute',
        left: window.innerWidth < 720 ? '10px' : '20px',
    },
    routesWrapper: {
        width: `${window.innerWidth}px`,
        padding: '0.5em',
        backgroundColor: '#1e3565',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'nowrap',
    },
    menuRightWrapper: {
        width: '100px',
        position: 'absolute',
        right: '-100px',
        top: '48px',
        display: 'flex',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: Colors.appColor,
        height: '96px',
        zIndex: '1',
    },
    signOut: {
        backgroundColor: Colors.appColor,
        borderRadius: '2%',
    },

    route: {},
};

const mapStateToProps = state => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        signOut: () => dispatch(UserActions.signOut()),
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(Navbar),
);
