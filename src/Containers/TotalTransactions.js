import React from 'react';
import { connect } from 'react-redux';
import { Metrics, Colors } from '../Utils';
import { injectIntl } from 'react-intl';
import { GridLoader, SyncLoader } from 'react-spinners';
import HistoryActions from '../Redux/History/HistoryRedux';
import HistoryTransaction from '../Components/HistoryTransaction';
import { Button } from 'react-bootstrap';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

class TotalTransactions extends React.Component {
    dontShowSpinner = false;
    limitReached = false;
    componentWillReceiveProps(nextProps) {
        const { success, transactions } = nextProps;
        if (success && !this.props.success) {
            if (transactions && this.props.transactions && transactions.length === this.props.transactions.length) {
                this.limitReached = true;
            }
            this.dontShowSpinner = false;
        }
    }

    renderLoadingScreen = () => (
        <div
            style={{
                height: window.innerWidth < 720 ? 'auto' : Metrics.screenHeight - 400,
                justifyContent: 'center',
                display: ' flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: this.props.width,
            }}
        >
            <GridLoader color={Colors.appColor} loading={true} />
            <p
                style={{
                    marginTop: '1em',
                    fontSize: '20px',
                    fontWeight: '500',
                    left: '1em',
                }}
            >
                {this.props.intl.messages.loadingPoints}
            </p>
        </div>
    );

    renderNoPoints = () => (
        <div
            style={{
                height: window.innerWidth < 720 ? 'auto' : Metrics.screenHeight - 340,
                justifyContent: 'center',
                display: ' flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: this.props.width,
            }}
        >
            <p
                style={{
                    marginTop: '1em',
                    fontSize: '20px',
                    fontWeight: '500',
                    left: '1em',
                }}
            >
                {this.props.intl.messages.noPoints}
            </p>
        </div>
    );

    renderTransactions = () => {
        return this.props.transactions && this.props.transactions.map(transaction => ReactHtmlParser(transaction));
    };

    loadMore = () => {
        this.dontShowSpinner = true;
        this.props.getHistoryRequest();
    };

    render() {
        let content = null;
        const fetching = this.props.fetching || (!this.props.transactions && !this.props.errorMessage);
        const nothingToDisplay = !this.props.transactions || !this.props.transactions.length;

        if (fetching && !this.dontShowSpinner) {
            content = this.renderLoadingScreen();
        } else if (nothingToDisplay) {
            content = this.renderNoPoints();
        } else {
            content = this.renderTransactions();
        }
        const matches = window.innerWidth < 720;
        return (
            <div style={{ ...styles.container, width: this.props.width }}>
                <p style={styles.title}>{this.props.intl.messages.history}</p>
                {content}
            </div>
        );
    }
}

const styles = {
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
    },
    headerText: {
        margin: 0,
        fontSize: '16px',
        fontWeight: '400',
    },
    header: {
        flexFlow: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        display: 'flex',
        padding: '0 .7em',
    },
    container: {
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexFlow: 'column',
        display: 'flex',
        padding: '1em .6em',
    },
};

const mapStateToProps = state => {
    const history = state.history || {};
    return {
        transactions: history.total,
        fetching: history.fetching && !history.totalErrorMessage,
        success: history.success,
        errorMessage: history.totalErrorMessage || history.errorMessage,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getHistoryRequest: () => dispatch(HistoryActions.getHistoryRequest()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(injectIntl(TotalTransactions));
