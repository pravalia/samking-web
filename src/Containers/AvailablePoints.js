import React from 'react';
import { connect } from 'react-redux';
import { Metrics, Colors } from '../Utils';
import { injectIntl } from 'react-intl';
import { GridLoader } from 'react-spinners';
import { Button } from 'react-bootstrap';
import HistoryActions from '../Redux/History/HistoryRedux';
import Transaction from '../Components/Transaction';

class AvailablePoints extends React.Component {
    state = {
        noMoreTransactions: false,
    };

    componentWillReceiveProps(nextProps) {
        const { success, transactions } = nextProps;
        if (success) {
            if (!this.state.noMoreTransactions && this.props.transactions && transactions.length === this.props.transactions.length) {
                this.setState({
                    noMoreTransactions: true,
                });
            }
        }
    }

    loadMore = () => {
        this.props.getHistoryRequest();
    };
    renderLoadingScreen = () => (
        <div
            style={{
                height: window.innerWidth < 720 ? 'auto' : Metrics.screenHeight - 400,
                justifyContent: 'center',
                display: ' flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: this.props.width,
            }}
        >
            <GridLoader color={Colors.appColor} loading={true} />
            <p
                style={{
                    fontFamily: 'Verdana',
                    marginTop: '0.3em',
                    fontSize: '20px',
                    fontWeight: '500',
                    left: '1em',
                }}
            >
                {this.props.intl.messages.loadingPoints}
            </p>
        </div>
    );

    renderNoPoints = () => (
        <div
            style={{
                height: window.innerWidth < 720 ? 'auto' : Metrics.screenHeight - 340,
                justifyContent: 'center',
                display: ' flex',
                alignItems: 'center',
                flexDirection: 'column',
                width: this.props.width,
            }}
        >
            <p
                style={{
                    fontFamily: 'Verdana',
                    marginTop: '1em',
                    fontSize: '20px',
                    fontWeight: '500',
                    left: '1em',
                }}
            >
                {this.props.intl.messages.noPoints}
            </p>
        </div>
    );
    renderLoadMore = () => {
        if (!this.state.noMoreTransactions) {
            return (
                <Button bsStyle="info" style={{ marginTop: '1em' }} onClick={this.loadMore}>
                    {this.props.intl.messages.loadMore}
                </Button>
            );
        }
    };

    getRanking = () => {
        const { ranking, intl } = this.props;
        const width = this.props.width - 30;
        const rankingWidth = width / 2.5;
        if (ranking == null) return;
        const { vouchers, youHave, points, rank } = intl.messages;
        const { NumberOfCompanies, NumberOfPoints, RankCompanies, RankPoints } = ranking;
        return (
            <div style={styles.rankingWrapper}>
                <div style={{ ...styles.rankingRow, width }}>
                    <p style={{ ...styles.ranking, width: rankingWidth }}>
                        {`${youHave}
                        ${NumberOfPoints} ${points}`}
                    </p>
                    <p style={{ ...styles.ranking, width: rankingWidth }}>{`${rank} ${RankPoints}`}</p>
                </div>
            </div>
        );
    };

    renderTransactions = () => {
        const { transactions, width } = this.props;
        return transactions.map((transaction, idx) => (
            <Transaction useMinWidth width={width} key={transaction.CompanyName + idx} transaction={transaction} intl={this.props.intl} showForFree />
        ));
    };

    render() {
        const { intl } = this.props;
        let content = null;
        const fetching = this.props.fetching || (!this.props.transactions && !this.props.errorMessage);
        const nothingToDisplay = !this.props.transactions || !this.props.transactions.length;

        if (fetching) {
            content = this.renderLoadingScreen();
        } else if (nothingToDisplay) {
            content = this.renderNoPoints();
        } else {
            content = this.renderTransactions();
        }
        const header = !fetching &&
            !nothingToDisplay && (
                <div
                    style={{
                        ...styles.header,
                        width: `${this.props.width - 30}px`,
                    }}
                >
                    <p
                        style={{
                            ...styles.headerText,
                            maxWidth: this.props.width / 3,
                        }}
                    >
                        {intl.messages.companyName}
                    </p>
                    <p
                        style={{
                            ...styles.headerText,
                            maxWidth: this.props.width / 3,
                        }}
                    >
                        {intl.messages.value}
                    </p>
                </div>
            );
        return (
            <div style={{ ...styles.container, width: this.props.width }}>
                <p style={styles.title}>{this.props.intl.messages.pointsAwarded}</p>
                {header}
                {content}
            </div>
        );
    }
}

const styles = {
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
    },
    headerText: {
        margin: 0,
        fontSize: '15px',
        fontWeight: '400',
    },
    header: {
        flexFlow: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        display: 'flex',
        padding: '0 .7em',
    },
    container: {
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
        flexFlow: 'column',
        display: 'flex',
        padding: '1em 2em',
    },
    rankingWrapper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '1em',
    },
    rankingRow: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: '1em',
    },
    ranking: {
        backgroundColor: '#1e3565',
        padding: '.7em',
        color: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0,
        textAlign: 'center',
    },
};

const mapStateToProps = state => {
    const history = state.history || {};
    return {
        transactions: history.history,
        ranking: history.ranking,
        fetching: history.fetching,
        success: history.success,
        errorMessage: history.errorMessage,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getHistoryRequest: () => dispatch(HistoryActions.getHistoryRequest()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(injectIntl(AvailablePoints));
