import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import PasswordActions from '../Redux/User/PasswordRedux';
import { Metrics, Colors } from '../Utils';
import { Form } from 'react-form';
import { withAlert } from 'react-alert';
import { Redirect } from 'react-router-dom';
import { Button, FormGroup, ControlLabel, FormControl, Checkbox } from 'react-bootstrap';

class ResetPasswordContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            key: window.location.hash.substr(window.location.hash.indexOf('?key=') + 5),
            hidePasswords: true,
            goBack: false,
            newPassword: '',
            repeatPassword: '',
            newPasswordError: '',
            repeatPasswordError: '',
        };
    }

    componentWillReceiveProps(nextProps) {
        const { success, errorMessage } = nextProps;
        if (success && !this.props.success) {
            this.props.alert.success(this.props.intl.messages.changePasswordSuccess, {
                onClose: () => this.setState({ goBack: true }),
            });
        }
        if (errorMessage && !this.props.errorMessage) {
            this.props.alert.error(errorMessage);
        }
        this.props.resetPasswordForm();
    }

    checkIfPasswordsMatch = () => {
        const { newPassword, repeatPassword } = this.state;
        const { intl } = this.props;
        const passwordsMatch = newPassword === repeatPassword;
        if (!passwordsMatch) {
            this.setState(prevState => {
                return {
                    newPasswordError: intl.messages.passwordsDontMatch,
                    repeatPasswordError: intl.messages.passwordsDontMatch,
                };
            });
        }
        return passwordsMatch;
    };

    checkForEmptyFields = () => {
        const { newPassword, repeatPassword } = this.state;
        const { intl } = this.props;
        this.setState({
            newPasswordError: newPassword ? '' : intl.messages.cannotBeBlank,
            repeatPasswordError: repeatPassword ? '' : intl.messages.cannotBeBlank,
        });
        return newPassword && repeatPassword;
    };

    onChange = (field, value) => {
        this.setState({ [field]: value });
    };

    onToggleHidePasswords = () => {
        this.setState(prevState => {
            return {
                hidePasswords: !prevState.hidePasswords,
            };
        });
    };

    onSubmit = () => {
        if (this.checkForEmptyFields() && this.checkIfPasswordsMatch()) {
            this.props.resetPasswordRequest({
                key: this.state.key,
                newPassword: this.state.newPassword,
            });
        }
    };

    goBack = () => {
        this.setState({ goBack: true });
    };

    render() {
        const { intl } = this.props;
        const { newPassword, repeatPassword, newPasswordError, repeatPasswordError } = this.state;
        if (this.state.goBack) {
            return <Redirect to="/login" />;
        }
        const matches = window.innerWidth < 720;
        return (
            <div
                style={{
                    marginTop: matches ? '0px' : '100px',
                    display: 'flex',
                    justifyContent: 'center',
                }}
            >
                <Form onSubmit={this.onSubmit}>
                    <form
                        style={{
                            ...styles.form,
                            padding: matches ? '15px 0' : '0px',
                        }}
                        id="updatePassword"
                    >
                        <p style={styles.newPasswordTitle}>{intl.messages.enterNewPassword}</p>
                        <div
                            style={{
                                marginTop: '5px',
                                display: 'flex',
                                flexFlow: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                        >
                            <FormGroup controlId="newPasswordGroup" validationState={newPasswordError ? 'error' : null}>
                                <ControlLabel>{newPasswordError ? newPasswordError : intl.messages.newPassword}</ControlLabel>
                                <FormControl
                                    type={this.state.hidePasswords ? 'password' : 'text'}
                                    value={newPassword}
                                    onChange={e => this.onChange('newPassword', e.target.value)}
                                />
                                <FormControl.Feedback />
                            </FormGroup>{' '}
                            <FormGroup controlId="repeatPasswordGroup" validationState={repeatPasswordError ? 'error' : null}>
                                <ControlLabel>{repeatPasswordError ? repeatPasswordError : intl.messages.repeatPassword}</ControlLabel>
                                <FormControl
                                    type={this.state.hidePasswords ? 'password' : 'text'}
                                    value={repeatPassword}
                                    onChange={e => this.onChange('repeatPassword', e.target.value)}
                                />
                                <FormControl.Feedback />
                            </FormGroup>
                            <FormGroup controlId="hidePasswords">
                                <Checkbox
                                    checked={this.state.hidePasswords}
                                    onChange={this.onToggleHidePasswords}
                                    className="signInCheckbox"
                                    style={styles.checkbox}
                                >
                                    {intl.messages.hidePasswords}
                                </Checkbox>
                            </FormGroup>
                            <div style={styles.formRow}>
                                <Button
                                    bsSize="lg"
                                    bsStyle="success"
                                    onClick={this.onSubmit}
                                    type="button"
                                    style={{
                                        width: matches ? '250px' : '300px',
                                        backgroundColor: Colors.appColor,
                                    }}
                                >
                                    {intl.messages.resetPasswordCTA}
                                </Button>
                            </div>
                        </div>
                    </form>
                </Form>
            </div>
        );
    }
}

const styles = {
    formWrapper: {
        display: 'flex',
        flexGrow: 1,
        marginTop: Metrics.baseMargin,
    },
    bottomCTAWrapper: {
        display: 'flex',

        justifyContent: 'center',
        alignContent: 'center',
    },
    checkbox: {
        display: 'flex',

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    newPasswordTitle: {
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: 'center',
        marginBottom: '36px',
    },
};

const mapStateToProps = state => {
    const passwordForm = state.passwordForm || {};
    return {
        errorMessage: passwordForm.errorMessage,
        success: passwordForm.success,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        resetPasswordRequest: payload => dispatch(PasswordActions.resetPasswordRequest(payload)),
        onPasswordFormFieldChange: (field, value) => dispatch(PasswordActions.onPasswordFormFieldChange(field, value)),
        resetPasswordForm: () => dispatch(PasswordActions.resetPasswordForm()),
        resetPasswordFormFlags: () => dispatch(PasswordActions.resetPasswordFormFlags()),
        checkforEmptyFields: intl => dispatch(PasswordActions.checkForEmptyFields(intl)),
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(withAlert(ResetPasswordContainer)),
);
