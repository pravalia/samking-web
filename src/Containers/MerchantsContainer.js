import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import { PropagateLoader } from 'react-spinners';
import MerchantsActions from '../Redux/Merchants/MerchantsRedux';
import AppActions from '../Redux/AppRedux';
import Map from '../Components/Map';
import { withAlert } from 'react-alert';
import { Metrics, Colors } from '../Utils';

const initialCoords = {
    lat: 52.520008,
    lng: 13.404954,
};
class MerchantsContainer extends React.Component {
    state = {
        coordsRejected: false,
        coords: initialCoords,
        prevEdgeCoords: initialCoords,
        permissionGranted: false,
    };
    componentDidMount() {
        document.getElementsByTagName('body')[0].style.overflowY = 'hidden';
        navigator.geolocation.getCurrentPosition(this.getMerchants, this.showRejectionError);
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.mapWatch);
        const isMobile = window.innerWidth < 720;
        document.getElementsByTagName('body')[0].style.overflowY = 'initial';
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.merchantsError && !this.props.merchantsError) {
            this.props.alert.error(this.props.intl.messages.apiErrors.server_unreachable, {
                onClose: () => this.setState({ coordsRejected: true }),
            });
        }
    }

    getMerchants = position => {
        const { latitude, longitude } = position.coords;
        const coords = { lat: latitude, lng: longitude };
        this.setState({
            coords,
            prevEdgeCoords: { lat: latitude, lng: longitude },
            permissionGranted: true,
        });
        this.mapWatch = navigator.geolocation.watchPosition(this.watchPosition);
        this.props.setUserCoords(coords);
        this.props.getMerchantsRequest(latitude, longitude, 13);
    };

    watchPosition = position => {
        const { latitude, longitude } = position.coords;
        const coords = {
            lat: latitude,
            lng: longitude,
        };
        this.setState(
            {
                coords,
            },
            this.maybeGetNewMerchants(position.coords),
        );
        this.props.setUserCoords(coords);
    };

    maybeGetNewMerchants = coords => {
        const { prevEdgeCoords } = this.state;
        const { lat, lng } = prevEdgeCoords;
        const { latitude, longitude } = coords;
        if (Math.abs(latitude - lat) > 0.3 || Math.abs(longitude - lng) > 0.5) {
            const zoom = this.map.getZoom();
            this.setState(
                {
                    prevEdgeCoords: {
                        lat: latitude,
                        lng: longitude,
                    },
                },
                () => this.props.getMerchantsRequest(latitude, longitude, zoom),
            );
        }
    };

    showRejectionError = () => {
        this.props.alert.error(this.props.intl.messages.coordsRejected, {
            onClose: () => this.setState({ coordsRejected: true }),
        });
    };

    render() {
        if (this.state.coordsRejected) {
            return <Redirect to="/home" />;
        }
        const onlyOneAdd = window.location.hash === '#/merchants';
        const adsHeight = onlyOneAdd ? 85 : 145;
        return (
            <div>
                {this.state.permissionGranted ? (
                    <Map
                        setRef={map => (this.map = map)}
                        myCoords={this.state.coords}
                        maybeGetNewMerchants={this.maybeGetNewMerchants}
                        defaultCenter={this.state.coords}
                        getMerchants={this.props.getMerchantsRequest}
                        loadingElement={
                            <div
                                style={{
                                    height: Metrics.screenHeight - adsHeight - Metrics.navbarHeight,
                                }}
                            />
                        }
                        containerElement={
                            <div
                                style={{
                                    height: Metrics.screenHeight - adsHeight - Metrics.navbarHeight,
                                    width: window.innerWidth,
                                }}
                            />
                        }
                        mapElement={<div style={{ height: `100%` }} />}
                        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWM9_q_-F_3dWiZRhIPRmXH-1lGERjy0M&v=3.exp&libraries=geometry,drawing,places"
                    />
                ) : (
                    <div
                        style={{
                            overflow: 'hidden',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                            flexFlow: 'column',
                            width: window.innerWidth,
                            height: Metrics.screenHeight - Metrics.navbarHeight - 100,
                        }}
                    >
                        <PropagateLoader color={Colors.appColor} loading={true} />
                        <p
                            style={{
                                marginTop: '1em',
                                fontSize: '20px',
                                fontWeight: '500',
                            }}
                        >
                            {this.props.intl.messages.waitingForPermissions}
                        </p>
                    </div>
                )}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        merchants: state.merchants,
        merchantsError: state.merchants ? state.merchants.errorMessage : '',
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getMerchantsRequest: (lat, lng, zoom) => dispatch(MerchantsActions.getMerchantsRequest(lat, lng, zoom)),
        setUserCoords: coords => dispatch(AppActions.setUserCoords(coords)),
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(withAlert(MerchantsContainer)),
);
