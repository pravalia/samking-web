import React from 'react';
import AvailablePoints from '../Containers/AvailablePoints';
import TotalTransactions from '../Containers/TotalTransactions';
import TransferForm from '../Components/Forms/TransferForm';
import { Button } from 'react-bootstrap';
import { Metrics } from '../Utils';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import AlertsContainer from './AlertsContainer';
import Rankings from './Rankings';
class ClientHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            screenName: 'total',
        };
    }

    componentDidMount() {
        document.getElementsByTagName('body')[0].style.overflowY = 'visible';
    }

    changeScreen = screenName => () => {
        this.setState({
            screenName,
        });
    };

    renderNavbar = () => {
        const buttonStyle = window.innerWidth < 720 ? styles.navbarButton : styles.desktopNavbar;
        return (
            <div style={styles.navbar}>
                <Button
                    style={{
                        ...buttonStyle,
                        backgroundColor: this.state.screenName === 'total' ? '#bf9557' : '#f4d29f',
                    }}
                    onClick={this.changeScreen('total')}
                >
                    {this.props.intl.messages.availableLabel}
                </Button>
                <Button
                    style={{
                        ...buttonStyle,
                        backgroundColor: this.state.screenName === 'alerts' ? '#bf9557' : '#f4d29f',
                    }}
                    onClick={this.changeScreen('alerts')}
                >
                    {this.props.intl.messages.alertsLabel}
                </Button>
                <Button
                    style={{
                        ...buttonStyle,
                        backgroundColor: this.state.screenName === 'history' ? '#bf9557' : '#f4d29f',
                    }}
                    onClick={this.changeScreen('history')}
                >
                    {this.props.intl.messages.historyLabel}
                </Button>

                {this.props.transactions && this.props.transactions.length ? (
                    <Button
                        style={{
                            ...buttonStyle,
                            backgroundColor: this.state.screenName === 'transfer' ? '#bf9557' : '#f4d29f',
                        }}
                        onClick={this.changeScreen('transfer')}
                    >
                        {this.props.intl.messages.transferLabel}
                    </Button>
                ) : null}
                <Button
                    style={{
                        ...buttonStyle,
                        backgroundColor: this.state.screenName === 'rankings' ? '#bf9557' : '#f4d29f',
                    }}
                    onClick={this.changeScreen('rankings')}
                >
                    {this.props.intl.messages.rankings}
                </Button>
            </div>
        );
    };

    render() {
        const isMobile = window.innerWidth < 720;
        const width = !isMobile ? window.innerWidth / 2 : window.innerWidth < 500 ? window.innerWidth : 500;
        return (
            <div style={styles.desktopWrapper}>
                {this.renderNavbar()}
                {this.state.screenName === 'history' && <TotalTransactions width={width} />}
                {this.state.screenName === 'alerts' && <AlertsContainer width={width} />}
                {this.state.screenName === 'total' && <AvailablePoints width={isMobile ? width : width * 1.3} />}
                {this.state.screenName === 'transfer' && <TransferForm width={width} />}
                {this.state.screenName === 'rankings' && <Rankings width={width} />}
            </div>
        );
    }
}

const mapStateToProps = state => {
    const history = state.history || {};
    return {
        transactions: history.history,
    };
};

const styles = {
    desktopWrapper: {
        display: 'flex',
        flexFlow: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    navbarButton: { margin: '10px 5px' },
    navbar: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        width: window.innerWidth,
    },
    desktopNavbar: {
        margin: '10px 5px',
        width: '180px',
        fontSize: '17px',
    },
};

export default connect(
    mapStateToProps,
    null,
)(injectIntl(ClientHistory));
