import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import UserFormActions from '../Redux/User/UserFormRedux';
import { Metrics, Colors } from '../Utils';
import { Form } from 'react-form';
import { withAlert } from 'react-alert';
import { Redirect } from 'react-router-dom';
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
class ForgotPasswordContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            emailAddress: '',
            goBack: false,
        };
    }

    componentWillReceiveProps(nextProps) {
        const { success, errorMessage } = nextProps;
        if (success && !this.props.success) {
            this.props.alert.success(`${this.props.intl.messages.resetPasswordEmailSent}${this.state.emailAddress}`, {
                onClose: () => this.setState({ goBack: true }),
            });
        }

        if (errorMessage && !this.props.errorMessage) {
            this.props.alert.error(errorMessage);
        }
    }

    componentWillMount() {
        this.props.resetUserForm();
    }

    componentWillUnmount() {
        this.props.resetUserForm();
    }

    onChange = value => {
        this.setState({ emailAddress: value });
    };

    onSubmit = () => {
        this.props.sendResetEmailRequest(this.state.emailAddress);
    };

    goBack = () => {
        this.setState({
            goBack: true,
        });
    };

    render() {
        const { intl } = this.props;
        if (this.props.user.id || this.state.goBack) {
            return <Redirect to="/login" />;
        }
        const matches = window.innerWidth < 720;

        return (
            <div
                style={{
                    marginTop: matches ? '0px' : '80px',
                    display: 'flex',
                    justifyContent: 'center',
                    border: '2px dotted rgba(0,0,0,0.2)',
                }}
            >
                <Form onSubmit={this.onSubmit}>
                    <form
                        style={{
                            ...styles.form,
                            padding: matches ? '15px 0' : '0px',
                        }}
                        id="updatePassword"
                    >
                        <div
                            style={{
                                marginTop: '5px',
                                display: 'flex',
                                flexFlow: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: '20px',
                            }}
                        >
                            <FormGroup controlId="emailGroup" validationState={null}>
                                <ControlLabel>{intl.messages.enterEmailAddress}</ControlLabel>
                                <FormControl type="text" value={this.state.email} onChange={e => this.onChange(e.target.value)} />
                                <FormControl.Feedback />
                            </FormGroup>

                            <div
                                style={{
                                    ...styles.formRow,
                                    flexDirection: 'column',
                                    display: 'flex',
                                }}
                            >
                                <Button
                                    bsSize="lg"
                                    bsStyle="success"
                                    onClick={this.onSubmit}
                                    type="button"
                                    style={{
                                        width: matches ? '250px' : '300px',
                                        backgroundColor: Colors.appColor,
                                        marginBottom: '16px',
                                    }}
                                >
                                    {intl.messages.sendEmail}
                                </Button>
                                <Button
                                    onClick={this.goBack}
                                    type="button"
                                    style={{
                                        width: matches ? '250px' : '300px',
                                        backgroundColor: Colors.appSecondaryColor,
                                    }}
                                >
                                    {intl.messages.backToLogin}
                                </Button>
                            </div>
                        </div>
                    </form>
                </Form>
            </div>
        );
    }
}

const styles = {
    formWrapper: {
        display: 'flex',
        flexGrow: 1,
        marginTop: Metrics.baseMargin,
    },
    bottomCTAWrapper: {
        display: 'flex',

        justifyContent: 'center',
        alignContent: 'center',
    },
    checkbox: {
        display: 'flex',

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
};

const mapStateToProps = state => {
    const { success, errorMessage } = state.passwordForm || {};
    return {
        user: state.user,
        success,
        errorMessage,
    };
};
const mapDispatchToProps = dispatch => {
    return {
        sendResetEmailRequest: email => dispatch(UserFormActions.sendResetEmailRequest(email)),
        resetUserForm: () => dispatch(UserFormActions.resetUserForm()),
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(withAlert(ForgotPasswordContainer)),
);
