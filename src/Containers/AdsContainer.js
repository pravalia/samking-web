import React from 'react';
import { connect } from 'react-redux';
const PAGES_TO_HIDE_ADS = ['#/login', '#/signup'];
const PAGES_TO_SHOW_ONLY_LOWER_AD = ['#/merchants'];

class AdsContainer extends React.Component {
    state = {
        hash: window.location.hash,
    };
    componentDidMount() {
        window.addEventListener('hashchange', e => {
            const { hash } = window.location;
            const queryIndex = hash.indexOf('?');
            const parsedHash = queryIndex > 0 ? hash.substr(0, queryIndex) : hash;
            this.setState({ hash: parsedHash });
        });
    }

    preventRender = () => {
        return PAGES_TO_HIDE_ADS.includes(this.state.hash) || (this.props.secondary && PAGES_TO_SHOW_ONLY_LOWER_AD.includes(this.state.hash));
    };
    render() {
        if (this.preventRender()) {
            return null;
        }
        return (
            <div
                style={{
                    ...footerStyle,
                    marginTop: this.props.secondary ? '0px' : '3px',
                }}
            >
                <div style={phantomStyle}>
                    <img
                        alt={'ad'}
                        style={{
                            height: '60px',
                            width: '375px',
                        }}
                        src={this.props.adSource}
                    />
                </div>
            </div>
        );
    }
}

const footerStyle = {
    backgroundColor: 'transparent',
    color: 'white',
    textAlign: 'center',
    left: '0',
    bottom: '0',
    width: '100%',
    pointerEvents: 'none',
};

const phantomStyle = {
    display: 'block',
    width: '100%',
    pointerEvents: 'none',
};

const mapStateToProps = (state, props) => {
    const { secondary } = props;
    const { app } = state;
    if (!app) {
        return { adSource: '' };
    }
    const { ads } = app;
    const adSource = secondary ? ads[1] : ads[0];
    return { adSource };
};

export default connect(
    mapStateToProps,
    null,
)(AdsContainer);
