import React from 'react';
import * as R from 'ramda';
import { connect } from 'react-redux';
import UserFormActions from '../Redux/User/UserFormRedux';
import UserActions from '../Redux/User/UserRedux';
import UserForm from '../Components/Forms/UserForm';
import { injectIntl } from 'react-intl';
import { Redirect } from 'react-router-dom';
import { withAlert } from 'react-alert';
import { Metrics } from '../Utils';

class EditProfileContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToHome: false
        };
    }

    componentDidMount() {
        const { initializeForm, user } = this.props;
        if (!user.countries || user.countries.length < 1) {
            this.props.alert.error(
                this.props.intl.messages.apiErrors.server_error,
                {
                    onClose: () => this.setState({ redirectToHome: true })
                }
            );
        }
        initializeForm(user);
    }

    componentWillReceiveProps(nextProps) {
        const { success, errorMessage } = nextProps.userForm;
        if (success && !this.props.userForm.success) {
            this.props.alert.success(
                this.props.intl.messages.updateProfileSuccess,
                { onClose: this.redirectToHome }
            );
        } else if (errorMessage && !this.props.userForm.errorMessage) {
            this.props.alert.error(errorMessage);
        }
    }

    redirectToHome = () => {
        this.setState({
            redirectToHome: true
        });
    };

    onChange = (values, path) => {
        const field = R.last(path);
        const value = R.path(path, values);
        this.props.onUserFormFieldChange(field, value);
    };

    onSubmit = fields => {
        this.props.updateUserRequest(fields);
    };

    render() {
        if (this.state.redirectToHome) {
            return <Redirect to="/home" />;
        }
        return (
            <div style={styles.formWrapper}>
                <p style={styles.title}>
                    {this.props.intl.messages.updateProfile}
                </p>
                <UserForm onSubmit={this.onSubmit} onChange={this.onChange} />
            </div>
        );
    }
}

const styles = {
    formWrapper: {
        flexGrow: 1,
        marginBottom: 0
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: '10px'
    },
    bottomCTAWrapper: {
        marginBottom: `${Metrics.baseMargin}px`,
        justifyContent: 'center',
        alignContent: 'center'
    }
};

const mapStateToProps = state => {
    return {
        userForm: state.userForm,
        user: state.user
    };
};
const mapDispatchToProps = dispatch => {
    return {
        initializeForm: user => dispatch(UserFormActions.initializeForm(user)),
        updateUserRequest: user =>
            dispatch(UserFormActions.updateUserRequest(user)),
        resetUserForm: () => dispatch(UserFormActions.resetUserForm()),
        setLanguage: language => dispatch(UserActions.setLanguage(language))
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withAlert(EditProfileContainer))
);
