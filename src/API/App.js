const create = api => {
    const getAds = (lat, lng) => {
        return api.get(`/ads?lat=${lat}&lng=${lng}`);
    };

    return { getAds };
};

export default create;
