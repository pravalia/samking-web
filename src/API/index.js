import apisauce from 'apisauce';
import userCreate from './User';
import historyCreate from './History';
import merchantsCreate from './Merchants';
import appCreate from './App';
import { cloneDeep } from 'lodash';
const config = require('../config');

const defaultUrl = config.rootApiUrl;
const create = (baseURL = defaultUrl) => {
    const api = apisauce.create({
        baseURL,
        timeout: 30000,
        withCredentials: false,
        crossDomain: true,
        noCORS: true,
    });

    api.addMonitor(response => {
        //         console.log(response);
        //         const clonedResponse = cloneDeep(response);
        //         const { config, data, status, duration, ok, problem } = clonedResponse;
        //         const { headers, method, params, url } = config;
        //         const requestData = config.data;
        //         console.log(
        //             `
        // %c${method.toUpperCase()} %c${url}
        // %ctook %c${duration} %cmilliseconds
        // %cstatus: ${status}
        // %crequest headers: %o
        // request data: %o
        // request params: %o
        // response data: %o
        // response problem: %o
        //         `,
        //             'color: purple',
        //             'color: blue',
        //             'color: purple',
        //             'color: blue',
        //             'color: purple',
        //             `color: ${ok ? 'green' : 'red'}`,
        //             'color: purple',
        //             JSON.stringify(headers),
        //             requestData,
        //             params,
        //             data,
        //             problem,
        //         );
    });

    api.addResponseTransform(response => {
        // if (
        //     (response.problem && response.problem === 'TIMEOUT_ERROR') ||
        //     response.status === null
        // ) {
        //     response.status = 666;
        //     response.data = {
        //         errorMessage: I18n.t('server_unreachable'),
        //     };
        // }

        response.status = response.status || 500;
        if (response.status === 500) {
            response.data = { errorMessage: 'server_error' };
        }
    });

    const setHeader = (header, value) => api.setHeader(header, value);
    const addMonitor = fn => api.addMonitor(fn);
    const headers = api.headers;

    const userRequests = userCreate(api),
        merchantsRequest = merchantsCreate(api),
        historyRequests = historyCreate(api),
        appRequests = appCreate(api);

    return {
        setHeader,
        addMonitor,
        headers,
        ...userRequests,
        ...merchantsRequest,
        ...historyRequests,
        ...appRequests,
    };
};

export default { create };
