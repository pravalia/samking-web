const create = api => {
    return {
        signIn: credentials => {
            return api.post('/clients', credentials);
        },
        signUp: payload => {
            return api.post('/clients', payload);
        },
        sendResetEmail: email => {
            return api.get(`/passwords?email=${email}`);
        },
        resetPassword: payload => {
            return api.post('/passwords', payload);
        },
        updateUser: payload => {
            return api.patch(`/clients`, payload);
        },
        updatePassword: payload => {
            return api.patch(`/passwords`, payload);
        },
        getCountries: () => {
            return api.get('/countries');
        },
        getLanguages: () => {
            return api.get('/languages');
        },
    };
};

export default create;
