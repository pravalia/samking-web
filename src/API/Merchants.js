const create = api => {
    const getMerchants = (lat, lng, zoom) => {
        return api.get(`/merchants?lat=${lat}&lng=${lng}&zoom=${zoom}`);
    };

    return { getMerchants };
};

export default create;
