const create = api => {
    const getHistory = (userId, offset, alerts, language) => {
        const transformedLanguage = transformLanguage(language);
        return api.get(
            `/history?id=${userId}&offset=${offset}&total=0&isAlert=0&language=${transformedLanguage}`
        );
    };
    const checkAvailableOffers = () => {
        return api.get('/history?offer=1');
    };
    const getHistoryTotal = (userId, offset, alerts, language) => {
        const transformedLanguage = transformLanguage(language);
        return api.get(
            `/history?id=${userId}&offset=${offset}&total=1&isAlert=${Number(
                alerts
            )}&language=${transformedLanguage}`
        );
    };
    const getRanking = () => {
        return api.get('/history');
    };
    const sendPoints = payload => {
        return api.post('/history', payload);
    };
    const removeAvailableOffer = id => {
        return api.patch(`/history?notificationId=${id}`);
    };

    return {
        getHistory,
        getHistoryTotal,
        sendPoints,
        getRanking,
        checkAvailableOffers,
        removeAvailableOffer
    };
};

const transformLanguage = language => {
    switch (language) {
        case 'Deutsch':
            return 'de';
        case 'Spanish':
            return 'es';
        default:
            return 'en';
    }
};

export default create;
