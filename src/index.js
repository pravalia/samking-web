/*global global */

import 'raf/polyfill';

import 'core-js';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider as StoreProvider } from 'react-redux';
import configureStore from './Redux/ConfigureStore';
require('es6-shim');

const { store, persistor } = configureStore();

ReactDOM.render(
    <StoreProvider store={store}>
        <PersistGate persistor={persistor}>
            {bootstrapped => (bootstrapped ? <App /> : <div />)}
        </PersistGate>
    </StoreProvider>,
    document.getElementById('root'),
);
