import React from 'react';
import { RingLoader } from 'react-spinners';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { Colors } from '../Utils';

const customStyles = {
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0, 0.9)',
    },
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: 'transparent',
        borderWidth: 0,
        width: '200px',
        height: '200px',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
    },
};

class Spinner extends React.Component {
    render() {
        return (
            <Modal
                isOpen={this.props.spinnerOpen}
                style={customStyles}
                contentLabel="Example Modal"
                ariaHideApp={false}
            >
                <RingLoader
                    color={Colors.appSecondaryColor}
                    loading={this.props.spinnerOpen}
                />
            </Modal>
        );
    }
}

const mapStateToProps = state => {
    return {
        spinnerOpen: state.app ? state.app.spinnerOpen : false,
    };
};

export default connect(mapStateToProps, null)(Spinner);
