import React from 'react';
import { Marker } from 'react-google-maps';

class MapMarker extends React.Component {
    onClick = e => {
        const validObj = this.getValidObj(e);
        const { clientY, clientX, layerX, layerY } = validObj;
        this.props.onClick({ clientX: clientX - layerX, clientY: clientY - layerY }, this.props.merchant.id);
    };

    getValidObj = e => {
        let object = {};
        Object.keys(e).forEach(key => {
            const property = e[key];
            if (typeof property === 'object') {
                if (this.hasKey(property)) {
                    object = property;
                } else {
                    const childReturn = this.getValidObj(property);
                    if (childReturn != null && Object.keys(childReturn).length > 0) {
                        object = childReturn;
                    }
                }
            }
        });
        return object;
    };

    hasKey = obj => {
        return obj.clientX != null;
    };

    render() {
        const { merchant } = this.props;
        const { lat, lng } = merchant;
        return (
            <Marker
                key={merchant.name + String(merchant.lat)}
                ref={marker => (this.marker = marker)}
                defaultIcon={this.props.source ? this.props.source : require('../Images/map-marker.png')}
                style={{ width: '30px', height: '35px' }}
                onClick={this.onClick}
                position={{ lat, lng }}
            />
        );
    }
}

export default MapMarker;
