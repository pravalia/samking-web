import React from 'react';
import { pick } from 'ramda';
import moment from 'moment';
import { Metrics, Colors } from '../Utils';

const mobile = window.availWidth < 720;
class Transaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            minWidth: props.width / 4
        };
    }

    getRow = (title, value, forFreeValue = '') => {
        const { total } = this.props;
        if (!value) return;
        let capitalizedTitle =
            title.substr(0, 1).toUpperCase() + title.substr(1);
        if (capitalizedTitle === 'CompanyName') {
            capitalizedTitle = 'Company Name';
        }
        if (title === 'value' || title === 'Amount') {
            value += ` ${this.props.intl.messages.points}`;
        }
        if (title && title.indexOf('Date') > 0) {
            const momentedValue = moment(value, 'yyyy-MM-DD');
            if (momentedValue._isValid) {
                value = momentedValue.format('DD/MM/YYYY');
            }
        }

        const hasFreeLabel = this.props.showForFree && Boolean(forFreeValue);

        const forFreeLabel = hasFreeLabel && (
            <p style={styles.forFreeLabel}>{`${forFreeValue} ${
                this.props.intl.messages.forFree
            }`}</p>
        );

        return (
            <div
                style={{
                    ...styles.row,
                    maxWidth: this.props.width * 0.7,
                    minWidth: this.props.useMinWidth ? this.state.minWidth : 0,
                    flexFlow: hasFreeLabel ? 'row nowrap' : 'column nowrap'
                }}
                key={title + value}
            >
                {total && (
                    <p style={styles.rowTitle}>
                        {this.props.intl.messages[capitalizedTitle] ||
                            capitalizedTitle}
                    </p>
                )}
                {forFreeLabel}
                <p style={styles.rowValue}>{value || 'N/A'}</p>
            </div>
        );
    };

    getTransactionsRows = () => {
        const { transaction } = this.props;
        const renderedRows = pick(
            [
                'CompanyName',
                'companyName',
                'Company',
                'value',
                'Amount',
                'validUntil',
                'AwardedDate',
                'AwardedPointOfSale',
                'RedeemedDate',
                'RedeemedPointOfSale',
                'TransferedOutDate',
                'TransferedOutPerson',
                'TransferedInDate',
                'TransferedInPerson'
            ],
            transaction
        );
        return Object.keys(renderedRows).map(rowKey => {
            const rowValue = renderedRows[rowKey];
            const forFreeValue = rowKey === 'Amount' && transaction.ForFree;
            return this.getRow(rowKey, rowValue, forFreeValue);
        });
    };

    render() {
        const rows = this.getTransactionsRows();
        return (
            <div
                style={{
                    ...styles.container,
                    backgroundColor: '#1e3565',
                    width: `${this.props.width - 30}px`
                }}
            >
                {rows}
            </div>
        );
    }
}

const styles = {
    container: {
        display: 'flex',
        flexFlow: 'row',
        flexWrap: 'nowrap',
        flex: 1,
        justifyContent:
            window.innerWidth < 720 ? 'space-between' : 'space-around',
        alignItems: 'center',
        width: `20em`,
        marginTop: '5px',
        color: 'white',
        padding: Metrics.isIE
            ? '2em'
            : window.innerWidth < 720
                ? '1em'
                : '0.7em'
    },
    row: {
        display: 'flex',
        flexFlow: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '.6em',
        minWidth: '2em'
    },
    rowTitle: {
        fontWeight: 'bold',
        fontColor: 'black',
        margin: 0
    },
    rowValue: {
        fontColor: 'black',
        margin: 0
    },
    forFreeLabel: {
        fontSize: '15px',
        color: Colors.appSecondaryColor,
        margin: '0 10px'
    }
};

export default Transaction;
