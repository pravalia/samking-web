import React from 'react';
import { pick, omit } from 'ramda';
import moment from 'moment';
import { Metrics, Colors } from '../Utils';

class HistoryTransaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wrapperHeight: 0,
        };
    }

    componentDidMount() {
        this.calculateWrapperHeight();
    }

    calculateWrapperHeight = () => {
        if (!this.wrapper) {
            return;
        }
        const { height } = this.wrapper.getBoundingClientRect();
        if (height !== this.state.wrapperHeight) {
            this.setState({
                wrapperHeight: height,
            });
        }
    };

    getRow = (title, value, forFreeValue = '') => {
        const { total } = this.props;
        if (!value) return;
        let capitalizedTitle = title.substr(0, 1).toUpperCase() + title.substr(1);
        if (capitalizedTitle === 'CompanyName') {
            capitalizedTitle = 'Company Name';
        }
        if (title === 'value' || title === 'Amount') {
            value += ` ${this.props.intl.messages.points}`;
        }
        if (title && title.indexOf('Date') > 0) {
            const momentedValue = moment(value, 'yyyy-MM-DD');
            if (momentedValue._isValid) {
                value = momentedValue.format('DD/MM/YYYY');
            }
        }

        const hasFreeLabel = this.props.showForFree && Boolean(forFreeValue);

        const forFreeLabel = hasFreeLabel && <p style={styles.forFreeLabel}>{`${forFreeValue} ${this.props.intl.messages.forFree}`}</p>;

        return (
            <div
                style={{
                    ...styles.row,
                    maxWidth: this.props.width * 0.7,
                    flexFlow: hasFreeLabel ? 'row nowrap' : 'column nowrap',
                }}
                key={title + value}
            >
                {total && <p style={styles.rowTitle}>{this.props.intl.messages[capitalizedTitle] || capitalizedTitle}</p>}
                <p style={styles.rowValue}>{value || 'N/A'}</p>
                {forFreeLabel}
            </div>
        );
    };

    extractTrace = (collection, traceHook) => {
        const omittedItems = Object.keys(collection).filter(item => !item.includes(traceHook));
        const traceItems = omit(omittedItems, collection);
        const traceEntry = {};
        traceEntry.message = this.props.intl.messages.traceHooks[traceHook];
        const company = traceItems[Object.keys(traceItems).find(key => key.includes('PointOfSale'))];
        const date = traceItems[Object.keys(traceItems).find(key => key.includes('Date'))];
        if (company == null || date == null) {
            return null;
        }
        traceEntry.company = company;
        traceEntry.date = moment(date, 'YYYY-MM-DD').format('DD.MM.YYYY');
        return traceEntry;
    };

    getExpiresInfo = transaction => {
        const { intl } = this.props;
        const { messages } = intl;
        const expiresDate = transaction.ValidUntil;
        const momentedValue = moment(expiresDate, 'YYYY-MM-DD');
        const now = moment();
        const expired = now.isAfter(momentedValue);
        const header = expired ? messages.traceHooks.expired : messages.traceHooks.validAnother;
        const footer = expired ? messages.traceHooks.daysAgo : messages.traceHooks.days;
        const days = Math.abs(momentedValue.diff(now, 'days'));
        return { expired, days, header, footer };
    };

    getTransactionsRows = () => {
        const { transaction } = this.props;
        const renderedRows = pick(
            [
                'CompanyName',
                'companyName',
                'Company',
                'value',
                'Amount',
                'validUntil',
                'AwardedDate',
                'AwardedPointOfSale',
                'RedeemedDate',
                'RedeemedPointOfSale',
                'TransferedOutDate',
                'TransferedOutPerson',
                'TransferedInDate',
                'TransferedInPerson',
            ],
            transaction,
        );
        const points = renderedRows.Amount;
        const awardedObject = this.extractTrace(transaction, 'Awarded');
        const redeemedObject = this.extractTrace(transaction, 'Redeemed');
        const transferedInObject = this.extractTrace(transaction, 'TransferedIn');
        const transferedOutObject = this.extractTrace(transaction, 'TransferedOut');
        const expiresObject = this.getExpiresInfo(transaction);
        if (!awardedObject && !redeemedObject && !transferedInObject && !transferedOutObject) {
            return null;
        }
        return (
            <React.Fragment>
                {this.renderPointsView(points)} {this.renderHistoryTrace(awardedObject)} {this.renderHistoryTrace(redeemedObject)}
                {this.renderHistoryTrace(transferedInObject)} {this.renderHistoryTrace(transferedOutObject)} {this.renderAvailability(expiresObject)}
            </React.Fragment>
        );
    };

    renderPointsView = points => {
        const { intl } = this.props;
        return (
            <div style={{ ...styles.flexibleWrapper, height: this.state.wrapperHeight, left: 0 }}>
                <div style={styles.pointsWrapper}>
                    <p style={styles.simpleText}>{intl.messages.points}</p>
                    <p style={styles.expiresDays}>{points}</p>
                    <p style={{ ...styles.simpleText, visibility: 'hidden' }}>{` placeholder `}</p>
                </div>
            </div>
        );
    };

    renderHistoryTrace = trace => {
        if (trace == null) {
            return null;
        }
        const { message, company, date } = trace;
        return (
            <div style={styles.traceWrapper}>
                <p style={styles.simpleText}>{message}</p>
                <p style={{ ...styles.traceInfo, textAlign: 'center' }}>{company}</p>
                <p style={styles.traceInfo}>{date}</p>
            </div>
        );
    };

    renderAvailability = availability => {
        const { header, days, footer, expired } = availability;
        const color = expired ? Colors.error : 'seagreen';
        return (
            <div style={{ ...styles.flexibleWrapper, height: this.state.wrapperHeight, backgroundColor: color, right: 0 }}>
                <div style={styles.expiresWrapper}>
                    <p style={styles.simpleText}>{header}</p>
                    <p style={styles.expiresDays}>{days}</p>
                    <p style={styles.simpleText}>{footer}</p>
                </div>
            </div>
        );
    };

    render() {
        const rows = this.getTransactionsRows();
        return rows ? (
            <div
                ref={wrapper => (this.wrapper = wrapper)}
                style={{
                    ...styles.container,
                    backgroundColor: '#1e3565',
                    width: `${this.props.width - 30}px`,
                }}
            >
                {rows}
            </div>
        ) : null;
    }
}

const styles = {
    pointsWrapper: {
        display: 'flex',
        flexFlow: 'column nowrap',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: window.innerWidth < 720 ? '80px' : '120px',
    },
    flexibleWrapper: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
    },

    traceWrapper: {
        display: 'flex',
        flexFlow: 'column nowrap',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '10px',
        minWidth: window.innerWidth < 720 ? '110px' : '150px',
    },
    expiresWrapper: {
        display: 'flex',
        flexFlow: 'column nowrap',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: window.innerWidth < 720 ? '130px' : '180px',
    },
    expiresDays: {
        fontSize: '20px',
        color: Colors.appSecondaryColor,
        margin: '2px 0px',
    },
    traceInfo: {
        fontSize: '16px',
        margin: '2px 0',
    },
    simpleText: {
        margin: '2px 0px',
    },
    container: {
        display: 'flex',
        flexFlow: 'row',
        flexWrap: 'wrap',
        position: 'relative',
        flex: 1,
        justifyContent: window.innerWidth < 720 ? 'space-between' : 'flex-start',
        alignItems: 'center',
        marginTop: '5px',
        color: 'white',
        padding: Metrics.isIE ? '2em' : window.innerWidth < 720 ? '1em' : '0 0.7em',
        paddingRight: window.screen.innerWidth < 720 ? '120px' : '200px',
        paddingLeft: window.innerWidth < 720 ? '80px' : '120px',
    },
    row: {
        display: 'flex',
        flexFlow: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: '.6em',
        minWidth: '2em',
    },
    rowTitle: {
        fontWeight: 'bold',
        fontColor: 'black',
        margin: 0,
    },
    rowValue: {
        fontColor: 'black',
        margin: 0,
    },
    forFreeLabel: {
        fontSize: '15px',
        color: Colors.appSecondaryColor,
        margin: '0 10px',
    },
};

export default HistoryTransaction;
