import React from 'react';
import { Metrics, AppStyles } from '../Utils';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

class ClientDetails extends React.PureComponent {
    render() {
        const { user, intl } = this.props;
        const hasUser = user && user.id;
        const matches = window.innerWidth < 720;
        const { firstName, lastName, displayName } = user;
        const name = displayName ? displayName : `${firstName} ${lastName}`;
        return !hasUser ? (
            <div />
        ) : (
            <div style={styles.container}>
                <p
                    style={{
                        ...AppStyles.controls.generalText,
                        ...styles.row,
                        fontSize: matches ? '14px' : '17px'
                    }}
                >{`${intl.messages.welcome}, ${name}`}</p>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const styles = {
    container: {
        display: 'flex',
        width: window.innerWidth,
        paddingTop: 5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    row: { borderRightWidth: 0.5, paddingRight: 10, margin: '0px' }
};

export default injectIntl(
    connect(
        mapStateToProps,
        null
    )(ClientDetails)
);
