import React from 'react';
import { Metrics } from '../Utils';
import { Button } from 'react-bootstrap';

class MerchantDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMobileOffers: false,
        };
    }

    renderOffers = offers =>
        offers &&
        offers.map((offer, idx) => (
            <div key={offer.name + idx} style={styles.offersContentLine}>
                <div style={styles.offersContentLineName}>{`${offer.name}${
                    offer.redeemCost ? '(' + offer.redeemCost + ' ' + this.props.intl.messages.redeemValue + ')' : ''
                }`}</div>
                <div style={styles.offersContentLineValue}>
                    {`${offer.numberOfPoints}
                    ${window.innerWidth < 720 ? this.props.intl.messages.pointsPerItem : ''}`}
                </div>
            </div>
        ));

    onChangeMobileView = () => {
        this.setState(prevState => {
            return {
                showMobileOffers: !prevState.showMobileOffers,
            };
        });
    };

    renderForMobile = () => {
        const { merchant, intl } = this.props;
        const { showMobileOffers } = this.state;
        return (
            <div
                style={{
                    ...styles.merchantDetails,
                    ...styles.merchantDetailsMobile,
                }}
            >
                {!showMobileOffers && (
                    <div
                        style={{
                            ...styles.merchantWrapper,
                            ...styles.merchantWrapperMobile,
                        }}
                    >
                        <p style={styles.detailsText}>{merchant.name}</p>
                        <p style={styles.detailsText}>
                            {merchant.points} {intl.messages.points}
                        </p>
                    </div>
                )}

                {showMobileOffers && (
                    <div
                        style={{
                            ...styles.offersWrapper,
                            ...styles.mobileOffersWrapper,
                            paddingBottom: '132px',
                        }}
                    >
                        <p style={styles.offersLabel}>{intl.messages.offers}</p>
                        {this.renderOffers(merchant.offers)}
                    </div>
                )}
                <Button style={styles.button} bsSize="sm" bsStyle="default" onClick={this.onChangeMobileView} type="button">
                    {this.state.showMobileOffers ? intl.messages.merchantDetails : intl.messages.offers}
                </Button>
            </div>
        );
    };

    renderForDesktop = () => {
        const { merchant, intl } = this.props;

        return (
            <div name="wrapper">
                <div style={styles.merchantDetails}>
                    <div>
                        <div name="header-details" style={styles.headerDetails}>
                            <div name="header-details-name" style={styles.headerDetailsName}>
                                {merchant.name}
                            </div>
                            <div name="address" style={styles.headerDetailsAddress}>
                                {merchant.address}
                            </div>
                            <div name="points" style={styles.headerDetailsPointsNumber}>
                                {merchant.points}
                                <div name="pts" style={styles.headerDetailsPointsPts}>
                                    {intl.messages.points}
                                </div>
                            </div>
                        </div>
                        <div name="offers-details" style={styles.offersDetails}>
                            <div>
                                <div name="offers-header" style={styles.offersHeader}>
                                    <div name="offers-header-offers" style={styles.offersHeaderOffers}>
                                        {intl.messages.offers}
                                    </div>

                                    <div name="offers-header-pts-item" style={styles.offersHeaderPtsItem}>
                                        {intl.messages.pointsPerItem}
                                    </div>
                                </div>
                                <div name="offers-content">{this.renderOffers(merchant.offers)}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    render() {
        const isMobile = window.innerWidth < 720;
        return isMobile ? this.renderForMobile() : this.renderForDesktop();
    }
}

const styles = {
    merchantDetails: {
        position: 'absolute',
        color: 'white',
        right: '2em',
        top: '7em',
        width: '15em',
        minHeight: '10em',
        maxHeight: '30em',
        overflow: 'auto',
        backgroundColor: '#1e3565',
        display: 'block',
        borderRadius: '3%',
        opacity: '1',
        padding: '2px',
        fontSize: '1em',
    },

    merchantDetailsMobile: {
        padding: '0.5em 0',
        left: '0',
        bottom: '0',
        top: 'auto',
        flexFlow: 'row',
        width: window.innerWidth,
        maxWidth: `${window.innerWidth}px`,
        height: '100px',
        minHeight: 'auto',
        borderRadius: '2%',
    },

    headerDetails: {
        padding: '0.5em',
        backgroundColor: 'black',
        position: 'relative',
    },

    headerDetailsName: {
        fontSize: '1em',
        fontWeight: '500',
        margin: '0px',
        display: 'block',
        maxWidth: '80%',
    },

    headerDetailsAddress: {
        fontSize: '0.7em',
        display: 'inline',
    },

    headerDetailsPointsNumber: {
        fontSize: '1.4em',
        margin: '0px',
        top: '0.3em',
        position: 'absolute',
        right: '0.3em',
        fontWeight: 'bold',
        color: 'orange',
    },

    headerDetailsPointsPts: {
        fontSize: '0.5em',
        float: 'right',
        fontWeight: 'normal',
        paddingLeft: '2px',
    },

    offersDetails: {
        padding: '3px 0.4em 0.7em',
    },

    offersHeader: {
        display: 'block',
        width: '100%',
        minHeight: '17px',
        fontSize: '0.8em',
        color: 'whitesmoke',
    },

    offersHeaderOffers: {
        float: 'left',
    },
    offersHeaderPtsItem: {
        float: 'right',
    },
    offersContent: {
        display: 'block',
    },
    offersContentLine: {
        display: 'block',
        minHeight: '1em',
        textAlign: 'left',
    },
    offersContentLineName: {
        margin: '0px',
        color: 'gold',
        display: 'inline-block',
    },
    offersContentLineValue: {
        margin: '0px',
        display: 'inline-block',
        float: 'right',
        textAlign: 'center',
    },

    /////
    offersWrapper: {
        border: '1px dotted white',
        padding: '.5em',
    },
    mobileOffersWrapper: {
        padding: '0em 2em',
        height: '90px',
        border: 'none',
    },
    button: {
        marginTop: '5px',
        marginBottom: '2px',
        marginLeft: '30%',
        width: '40%',
    },
    merchantWrapper: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    merchantWrapperMobile: {},
    offersLabel: {
        fontWeight: '500',
        fontSize: '18px',
        textAlign: 'center',
        margin: 0,
    },
    offerRow: {
        margin: 0,
        color: 'gold',
    },
    offerRowValue: {
        margin: 0,
    },
    detailsText: {
        fontSize: '16px',
        fontWeight: '500',
        margin: 0,
    },
    offer: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
};

export default MerchantDetails;
