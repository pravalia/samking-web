import React from 'react';
import ReactDOM from 'react-dom';
import { withGoogleMap, withScriptjs, GoogleMap } from 'react-google-maps';
import { connect } from 'react-redux';
import { Metrics } from '../Utils';
import MapMarker from '../Components/MapMarker';
import MerchantDetails from '../Components/MerchantDetails';
import { injectIntl } from 'react-intl';
const {
    MarkerClusterer,
} = require('react-google-maps/lib/components/addons/MarkerClusterer');

class Map extends React.Component {
    state = {
        clickCoords: {
            clientX: -100,
            clientY: -100,
            merchantId: 2,
        },
    };

    onMarkerClick = (e, merchantId) => {
        if (merchantId === this.state.merchantId) {
            return;
        }

        const { clientX, clientY } = e;
        this.setState({
            clickCoords: { clientX, clientY },
            merchantId,
        });
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot && snapshot.updateTooltip) {
            this.updateTooltipPosition();
        }
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevState.merchantId !== this.state.merchantId) {
            return { updateTooltip: true };
        }
        return null;
    }

    updateTooltipPosition = () => {
        const coords = ReactDOM.findDOMNode(
            this.tooltip,
        ).getBoundingClientRect();
        const markerCompensation = this.state.merchantId !== -1 ? 45 : 30;
        this.setState(prevState => {
            return {
                ...prevState,
                clickCoords: {
                    ...prevState.clickCoords,
                    clientX:
                        prevState.clickCoords.clientX -
                        (coords.width - markerCompensation) / 2,
                },
            };
        });
    };

    getMarkers = () => {
        const { merchants } = this.props;
        if (!merchants || !Array.isArray(merchants)) {
            return null;
        }
        return merchants.map(
            merchant =>
                merchant.id && (
                    <div
                        key={String(merchant.id)}
                        style={{ display: 'flex', background: 'blue' }}
                    >
                        <MapMarker
                            merchant={merchant}
                            onClick={this.onMarkerClick}
                        />
                    </div>
                ),
        );
    };

    closeLabel = () => {
        this.setState({
            merchantId: '',
            clickCoords: {
                clientX: -100,
                clientY: -100,
            },
        });
    };

    onDragEnd = () => {
        const center = this.googleMap.getCenter();
        const latitude = center.lat();
        const longitude = center.lng();
        this.props.maybeGetNewMerchants({ latitude, longitude });
    };

    onZoomChanged = e => {
        const zoom = this.googleMap.getZoom();
        const center = this.googleMap.getCenter();
        const lat = center.lat();
        const lng = center.lng();
        this.closeLabel();
        this.props.getMerchants(lat, lng, zoom);
    };

    render() {
        let merchant = this.props.merchants.find(
            merchant => merchant.id === this.state.merchantId,
        );
        if (this.state.merchantId === -1) {
            merchant = {
                name: this.props.intl.messages.yourLocation,
                id: -1,
            };
        }
        const merchantDetails = merchant &&
            merchant.id !== -1 && (
                <MerchantDetails intl={this.props.intl} merchant={merchant} />
            );
        const merchantAddress =
            merchant && merchant.address ? `, ${merchant.address}` : '';
        return (
            <GoogleMap
                ref={map => {
                    this.googleMap = map;
                    this.props.setRef(map);
                }}
                defaultZoom={13}
                defaultMaxZoom={200}
                onDragStart={this.closeLabel}
                onZoomChanged={this.onZoomChanged}
                onDragEnd={this.onDragEnd}
                {...this.props}
            >
                <MarkerClusterer defaultMaxZoom={20} onClick={this.closeLabel}>
                    {this.getMarkers()}
                </MarkerClusterer>
                <MapMarker
                    key={this.props.intl.messages.yourLocation}
                    source={require('../Images/my-location.png')}
                    onClick={this.onMarkerClick}
                    merchant={{
                        name: this.props.intl.messages.yourLocation,
                        ...this.props.myCoords,
                        id: -1,
                    }}
                />
                <div
                    ref={tooltip => (this.tooltip = tooltip)}
                    style={{
                        ...styles.labelWrapper,
                        top: `${this.state.clickCoords.clientY - 30}px`,
                        left: `${this.state.clickCoords.clientX}px`,
                        display: Metrics.isIE ? 'none' : 'block',
                    }}
                >
                    <div style={styles.arrow} />
                    <p style={styles.label}>
                        {merchant ? `${merchant.name}${merchantAddress}` : ''}
                    </p>
                </div>
                {merchantDetails}
            </GoogleMap>
        );
    }
}

const styles = {
    labelWrapper: {
        backgroundColor: '#e50000',
        color: 'white',
        fontSize: '15px',
        position: 'absolute',
        borderRadius: '5%',
    },
    label: {
        padding: '0 5px',
        margin: 0,
    },
    arrow: {
        borderLeft: '8px solid transparent',
        borderRight: '8px solid transparent',
        borderTop: '8px solid #e50000',
        position: 'absolute',
        bottom: '-7px',
        left: '50%',
        marginLeft: '-8px',
        right: '50%',
    },
};

const mapStateToProps = (state, props) => {
    return {
        merchants: state.merchants.merchants,
    };
};

export default withScriptjs(
    withGoogleMap(
        connect(
            mapStateToProps,
            null,
        )(injectIntl(Map)),
    ),
);
