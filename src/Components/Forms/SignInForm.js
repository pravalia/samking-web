/*eslint no-undef: "error"*/
import React from 'react';
import { Form } from 'react-form';
import { connect } from 'react-redux';
import UserActions from '../../Redux/User/UserRedux';
import { Metrics, Colors } from '../../Utils';
import { injectIntl } from 'react-intl';
import { Redirect, Link } from 'react-router-dom';
import { Button, FormGroup, ControlLabel, FormControl, Checkbox, DropdownButton, InputGroup, MenuItem } from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

class SignInForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keepSignedIn: false,
            dropdownOpen: false,
            user: props.user,
            fields: {
                email: '',
                password: '',
                languageId: props.user && props.user.languageId != null ? props.user.languageId : 1,
            },
        };
    }

    componentDidMount() {
        this.props.resetUserFlags();
        this.props.getLanguages();
        this.props.getCountries();
        this.enterListener = window.addEventListener('keydown', key => {
            if (key.keyCode === 13) {
                this.onSubmit();
            }
        });
    }

    static getDerivedStateFromProps(nextProps) {
        const { user } = nextProps;
        return {
            user,
        };
    }

    onSubmit = e => {
        this.props.signInRequest({
            ...this.state.fields,
            keepSignedIn: this.state.keepSignedIn,
        });
    };

    onClick = () => {
        const { email, password } = this.state.fields;
        if (email && password) {
            this.onSubmit();
        } else {
            this.setErrors();
        }
    };

    setErrors = () => {
        const errors = {};
        Object.keys(this.state.fields).forEach(key => {
            if (!this.state.fields[key]) {
                errors[`${key}Error`] = `${key} cannot be blank`;
            }
        });
        this.setState({
            fields: {
                ...this.state.fields,
                ...errors,
            },
        });
    };

    onChange = (field, value) => {
        this.setState(prevState => {
            return {
                user: {
                    ...prevState.user,
                    errorMessage: '',
                },
                fields: {
                    ...prevState.fields,
                    [field]: value,
                    emailError: '',
                    passwordError: '',
                },
            };
        });
    };

    toggleKeepSignedIn = () =>
        this.setState(prevState => {
            return {
                keepSignedIn: !prevState.keepSignedIn,
            };
        });

    onLanguageChange = language => {
        if (!language) {
            return;
        }
        const { value, label } = language;
        this.setState({
            dropdownOpen: false,
        });
        this.onChange('languageId', Number(value));
        this.props.setUserLanguage(label);
    };

    getLanguages = () => {
        const { languageId } = this.state.fields;
        return this.props.user.languages.map(({ name, id }) => ({
            value: id,
            label: name,
        }));
    };

    onToggleDropdown = () => {
        this.setState(prevState => {
            return {
                dropdownOpen: !this.state.dropdownOpen,
            };
        });
    };

    render() {
        const { intl } = this.props;
        const { user, fields } = this.state;
        const { emailError, passwordError } = fields;
        if (user.id) {
            return <Redirect to="/home" />;
        }
        const matches = window.innerWidth < 720;

        return (
            <div style={{ marginTop: matches ? '15px' : '40px' }}>
                <Form onSubmit={this.onSubmit}>
                    <form
                        style={{
                            ...styles.form,
                            ...isMobileStyles[matches].form,
                            padding: matches ? '15px 15px' : '0px',
                        }}
                        id="signInForm"
                    >
                        <img
                            src={require('../../Images/logo.png')}
                            style={{
                                width: '165px',
                                marginBottom: '-35px',
                                marginTop: '10px',
                            }}
                            alt="logo"
                        />

                        <div
                            style={{
                                marginTop: '50px',
                                ...isMobileStyles[matches].formRow,
                            }}
                        >
                            <FormGroup controlId="emailGroup" validationState={emailError ? 'error' : null}>
                                <ControlLabel>{emailError ? emailError : intl.messages.email}</ControlLabel>
                                <FormControl type="text" onChange={e => this.onChange('email', e.target.value)} />
                                <FormControl.Feedback />
                            </FormGroup>
                            <FormGroup controlId="passwordGroup" validationState={passwordError ? 'error' : null}>
                                <ControlLabel>{passwordError ? passwordError : intl.messages.password}</ControlLabel>
                                <FormControl type="password" onChange={e => this.onChange('password', e.target.value)} />
                                <FormControl.Feedback />
                            </FormGroup>
                            <p style={{ fontWeight: 'bold' }}>{intl.messages.language}</p>
                            <Select
                                name="countries"
                                value={this.state.fields.languageId}
                                onChange={this.onLanguageChange}
                                options={this.getLanguages()}
                                style={{ marginBottom: '16px' }}
                            />
                            <FormGroup controlId="keepSignedIn" style={styles.checkboxWrapper}>
                                <Checkbox
                                    checked={this.state.keepSignedIn}
                                    onChange={this.toggleKeepSignedIn}
                                    className="signInCheckbox"
                                    style={styles.checkbox}
                                >
                                    {intl.messages.keepSignedIn}
                                </Checkbox>
                            </FormGroup>
                            <div style={styles.linksWrapper}>
                                <Link to="/signup">{intl.messages.register}</Link>
                                <Link to="/forgotpassword">{intl.messages.forgotPassword}</Link>
                            </div>
                            {user.errorMessage && (
                                <span
                                    style={{
                                        ...styles.formRow,
                                        ...styles.error,
                                    }}
                                >
                                    {user.errorMessage}
                                </span>
                            )}
                            <div style={styles.formRow}>
                                <Button
                                    bsSize="lg"
                                    bsStyle="primary"
                                    onClick={() => this.onClick()}
                                    type="button"
                                    style={{
                                        width: matches ? '250px' : '300px',
                                        backgroundColor: Colors.appColor,
                                    }}
                                >
                                    {intl.messages.submit}
                                </Button>
                            </div>
                        </div>
                    </form>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        signInRequest: payload => dispatch(UserActions.signInRequest(payload)),
        resetUserFlags: () => dispatch(UserActions.resetUserFlags()),
        getCountries: () => dispatch(UserActions.getCountriesRequest()),
        getLanguages: () => dispatch(UserActions.getLanguagesRequest()),
        setUserLanguage: language => dispatch(UserActions.setLanguage(language)),
    };
};

const styles = {
    linksWrapper: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px 5px',
    },
    form: {
        maxWidth: window.innerWidth,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        display: 'flex',
        border: '2px dotted rgba(0,0,0,0.2)',
        marginTop: '5px',
    },
    formRow: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        display: 'flex',
        marginBottom: '1em',
    },
    formLabel: {
        display: 'flex',
        alignSelf: 'flex-start',
        marginLeft: '1.9em',
        marginBottom: '.3em',
    },
    checkboxWrapper: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    checkbox: {
        fontSize: '1.1em',
        fontWeight: 'bold',
    },
    inputField: {
        padding: '.5em .5em',
    },
    formError: {
        color: '#cc0000',
    },
    error: {
        backgroundColor: '#cc0000',
        color: 'white',
        paddingTop: '.5em',
        paddingBottom: '.5em',
        paddingLeft: '.3em',
    },
    button: {
        alignSelf: 'center',
    },
};

const isMobileStyles = {
    true: {
        form: {
            width: '22em',
        },
        formRow: {
            width: '20em',
        },
    },
    false: {
        form: {
            width: '30em',
        },
        formRow: {
            width: '300px',
        },
    },
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(SignInForm),
);
