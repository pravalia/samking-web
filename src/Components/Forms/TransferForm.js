import React from 'react';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import HistoryActions from '../../Redux/History/HistoryRedux';
import { Metrics, Colors } from '../../Utils';
import { Form } from 'react-form';
import { withAlert } from 'react-alert';
import {
    Button,
    FormGroup,
    ControlLabel,
    FormControl,
    DropdownButton,
    MenuItem,
    InputGroup
} from 'react-bootstrap';

const INITIAL_STATE = {
    email: '',
    numberOfPoints: '',
    companyId: -1,
    emailError: '',
    numberOfPointsError: ''
};

class TransferForm extends React.Component {
    constructor(props) {
        super(props);
        const firstTransaction = props.transactions
            ? props.transactions[0]
            : { CompanyId: -1 };

        this.state = {
            ...INITIAL_STATE,
            companyId: firstTransaction.CompanyId
        };
    }

    componentWillReceiveProps(nextProps) {
        const { success, errorMessage, transactions } = nextProps;
        if (
            transactions &&
            Array.isArray(transactions) &&
            transactions.length &&
            (!this.props.transactions ||
                transactions.length !== this.props.transactions.length)
        ) {
            this.setState({
                companyId: transactions[0].CompanyId
            });
        }
        if (success && !this.props.success) {
            this.props.alert.success(this.props.intl.messages.pointsSent, {
                onClose: () => {
                    this.setState(prevState => {
                        return {
                            ...INITIAL_STATE,
                            companyId: prevState.companyId
                        };
                    });
                    this.props.resetHistoryFlags();
                }
            });
        }
        if (errorMessage && !this.props.errorMessage) {
            this.props.alert.error(errorMessage);
        }
    }

    onChange = (field, value) => {
        this.setState({ [field]: value, [`${field}Error`]: '' });
    };

    checkForEmptyFields = () => {
        const { email, numberOfPoints, companyId } = this.state;
        const { intl } = this.props;
        this.setState({
            emailError: email ? '' : intl.messages.cannotBeBlank,
            numberOfPointsError: numberOfPoints
                ? ''
                : intl.messages.cannotBeBlank,
            companyIdError: companyId ? '' : intl.messages.cannotBeBlank
        });
        return email && companyId && numberOfPoints;
    };

    selectMerchant = id => {
        this.setState({
            companyId: id
        });
    };

    getMerchants = () => {
        return this.props.transactions
            ? this.props.transactions.map((transaction, idx) => {
                  const additionalStyle =
                      idx + 1 === this.props.transactions.length
                          ? {}
                          : {
                                borderBottomWidth: 1,
                                borderBottom: '1px dotted grey'
                            };
                  return (
                      <MenuItem
                          key={transaction.CompanyId}
                          onSelect={() =>
                              this.selectMerchant(transaction.CompanyId)
                          }
                          active={
                              this.state.companyId === transaction.CompanyId
                          }
                      >
                          <div
                              style={{ ...styles.menuItem, ...additionalStyle }}
                          >
                              <p style={styles.companyName}>
                                  {transaction.CompanyName}
                              </p>
                              <p style={styles.availablePoints}>
                                  {' '}
                                  {` ${
                                      this.props.intl.messages.availablePoints
                                  }: ${transaction.Amount}`}
                              </p>
                          </div>
                      </MenuItem>
                  );
              })
            : null;
    };

    onSubmit = () => {
        if (this.checkForEmptyFields()) {
            const { email, numberOfPoints, companyId } = this.state;
            if (email === this.props.user.email) {
                return this.setState({
                    emailError: this.props.intl.messages.cantSendToYourself
                });
            }
            this.props.sendPoints({
                email,
                numberOfPoints,
                companyId
            });
        }
    };

    render() {
        const { intl } = this.props;
        const {
            email,
            numberOfPoints,
            emailError,
            numberOfPointsError,
            companyIdError
        } = this.state;

        const matches = window.innerWidth < 720;

        const transactionWithCompany =
            this.props.transactions &&
            this.props.transactions.find(
                transaction => transaction.CompanyId === this.state.companyId
            );
        if (!transactionWithCompany) {
            return null;
        }
        const dropdownTitle = transactionWithCompany.CompanyName;

        return (
            <div
                style={{
                    ...styles.formContainer,
                    width: `${this.props.width}px`
                }}
            >
                <p style={styles.title}>
                    {this.props.intl.messages.sendPoints}
                </p>

                <div
                    style={{
                        marginTop: matches ? '0px' : '30px',
                        display: 'flex',
                        justifyContent: 'center'
                    }}
                >
                    <Form onSubmit={this.onSubmit}>
                        <form
                            style={{
                                ...styles.form,
                                padding: matches ? '0 0 10px 0' : '15px 0'
                            }}
                            id="updatePassword"
                        >
                            <div
                                style={{
                                    marginTop: '5px',
                                    maxWidth: matches ? '250px' : '350px'
                                }}
                            >
                                <FormGroup
                                    controlId="receiverEmailGroup"
                                    validationState={
                                        emailError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {emailError
                                            ? `${
                                                  intl.messages.receiverEmail
                                              }: ${emailError}`
                                            : intl.messages.receiverEmail}
                                    </ControlLabel>
                                    <FormControl
                                        type={'text'}
                                        value={email}
                                        onChange={e =>
                                            this.onChange(
                                                'email',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="companyNameGroup"
                                    validationState={
                                        companyIdError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {companyIdError
                                            ? `${
                                                  intl.messages.companyName
                                              }: ${companyIdError}`
                                            : intl.messages.companyName}
                                    </ControlLabel>
                                    <DropdownButton
                                        style={{
                                            zIndex: 0,
                                            width: matches ? '250px' : '350px'
                                        }}
                                        componentClass={InputGroup.Button}
                                        id="input-dropdown-addon"
                                        title={dropdownTitle}
                                    >
                                        {this.getMerchants()}
                                    </DropdownButton>
                                </FormGroup>{' '}
                                <FormGroup
                                    controlId="numberOfPointsGroup"
                                    validationState={
                                        numberOfPointsError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {numberOfPointsError
                                            ? `${
                                                  intl.messages.numberOfPoints
                                              }: ${numberOfPointsError}`
                                            : intl.messages.numberOfPoints}
                                    </ControlLabel>
                                    <FormControl
                                        type={'text'}
                                        value={numberOfPoints}
                                        onChange={e =>
                                            this.onChange(
                                                'numberOfPoints',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <div
                                    style={{
                                        ...styles.formRow,
                                        marginTop: matches ? 0 : '10px'
                                    }}
                                >
                                    <Button
                                        bsSize="lg"
                                        bsStyle="success"
                                        onClick={this.onSubmit}
                                        type="button"
                                        style={{
                                            width: matches ? '250px' : '350px',
                                            backgroundColor: Colors.appColor
                                        }}
                                    >
                                        {intl.messages.sendPoints}
                                    </Button>
                                </div>
                            </div>
                        </form>
                    </Form>
                </div>
            </div>
        );
    }
}

const styles = {
    formWrapper: {
        display: 'flex',
        flexGrow: 1,
        marginTop: Metrics.baseMargin
    },
    formContainer: {
        flexGrow: 1
    },
    title: {
        fontSize: '18px',
        fontWeight: 'bold',
        textAlign: 'center',
        margin: window.innerWidth < 720 ? '5px 0' : '10px 0'
    },
    companyName: {
        margin: 0,
        fontWeight: 'bold'
    },
    availablePoints: {
        margin: 0
    },
    bottomCTAWrapper: {
        display: 'flex',

        justifyContent: 'center',
        alignContent: 'center'
    },
    checkbox: {
        display: 'flex',

        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    menuItem: {
        width: window.innerWidth < 720 ? '200px' : '300px',
        display: 'flex',
        paddingBottom: '5px',

        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    }
};

const mapStateToProps = state => {
    const history = state.history || {};
    return {
        success: history.sendSuccess,
        errorMessage: history.sendErrorMessage,
        transactions: history.history,
        user: state.user
    };
};
const mapDispatchToProps = dispatch => {
    return {
        sendPoints: payload =>
            dispatch(HistoryActions.sendPointsRequest(payload)),
        resetHistoryFlags: () => dispatch(HistoryActions.resetHistoryFlags())
    };
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withAlert(TransferForm))
);
