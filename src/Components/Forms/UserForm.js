import React from 'react';
import { Form } from 'react-form';
import { connect } from 'react-redux';
import UserActions from '../../Redux/User/UserRedux';
import { Metrics, Colors } from '../../Utils';
import { injectIntl } from 'react-intl';
import { pick } from 'ramda';
import { Redirect } from 'react-router';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import {
    Button,
    FormGroup,
    ControlLabel,
    FormControl,
    DropdownButton,
    MenuItem,
    InputGroup
} from 'react-bootstrap';

class UserForm extends React.Component {
    constructor(props) {
        super(props);
        const { user } = props;
        const pickedFields = pick(
            [
                'firstName',
                'lastName',
                'phone',
                'address',
                'countryId',
                'city',
                'zipCode',
                'languageId',
                'displayName'
            ],
            user
        );
        this.state = {
            redirectToPasswordForm: false,
            dropdownOpen: false,
            countriesDropdownOpen: false,
            fields: {
                ...pickedFields,
                zipCode: pickedFields.zipCode || '',
                address: pickedFields.address || '',
                city: pickedFields.city || '',
                countryId: pickedFields.countryId || 1,
                languageId: pickedFields.languageId || 1,
                password: '',
                firstNameError: '',
                zipCodeError: '',
                lastNameError: '',
                displayNameError: '',
                countryIdError: '',
                zipCodeError: ''
            }
        };
    }

    onSubmit = () => {
        const {
            firstName,
            lastName,
            displayName,
            languageId,
            zipCode,
            city
        } = this.state.fields;
        if (
            firstName &&
            lastName &&
            displayName &&
            this.isValidCountry() &&
            languageId &&
            city &&
            zipCode
        ) {
            this.props.onSubmit(this.state.fields);
        } else {
            this.setErrors();
        }
    };

    isValidCountry = () => {
        return (
            this.props.user.countries.find(
                ({ id }) => id === this.state.fields.countryId
            ) != null
        );
    };

    setErrors = () => {
        const errors = {};
        Object.keys(this.state.fields).forEach(key => {
            if (key === 'countryId' && !this.isValidCountry()) {
                errors[`${key}Error`] = `${
                    this.props.intl.messages.selectValidCountry
                }`;
                return;
            }
            if (!this.state.fields[key] && !key.includes('Error')) {
                errors[`${key}Error`] = `${
                    this.props.intl.messages.cannotBeBlank
                }`;
            }
        });
        this.setState({
            fields: {
                ...this.state.fields,
                ...errors
            }
        });
    };

    onChange = (field, value) => {
        if (field === 'phone' && /\D/g.test(value)) {
            return;
        }
        this.setState(prevState => {
            return {
                fields: {
                    ...prevState.fields,
                    [field]: value,
                    firstNameError: '',
                    lastNameError: '',
                    phoneError: '',
                    countryIdError: '',
                    displayNameError: '',
                    zipCodeError: '',
                    cityError: ''
                }
            };
        });
    };

    onLanguageChange = language => {
        if (!language) {
            return;
        }
        const { value, label } = language;
        this.setState({
            dropdownOpen: false
        });
        this.onChange('languageId', Number(value));
        this.props.setUserLanguage(label);
    };

    onSelectCountry = country => {
        if (country == null) return;
        this.onChange('countryId', Number(country.value));
    };

    redirectToPasswordForm = () => {
        this.setState({
            redirectToPasswordForm: true
        });
    };

    getLanguages = () => {
        const { languageId } = this.state.fields;
        return this.props.user.languages.map(({ name, id }) => ({
            value: id,
            label: name
        }));
    };

    getCountries = () => {
        return this.props.user.countries.map(({ name, id }) => ({
            value: id,
            label: name
        }));
    };

    onToggleDropdown = () => {
        this.setState(prevState => {
            return {
                dropdownOpen: !prevState.dropdownOpen
            };
        });
    };

    onToggleCountriesDropdown = () => {
        this.setState(prevState => {
            return {
                countriesDropdownOpen: !prevState.countriesDropdownOpen
            };
        });
    };

    render() {
        const { intl, user } = this.props;
        const { fields, redirectToPasswordForm } = this.state;
        const countries = this.getCountries();
        const {
            firstName,
            lastName,
            phone,
            city,
            address,
            zipCode,
            displayName,
            countryId,
            languageId,
            firstNameError,
            lastNameError,
            displayNameError,
            countryIdError,
            zipCodeError,
            cityError
        } = fields;
        if (redirectToPasswordForm) {
            return <Redirect to="/changePassword" />;
        }
        const matches = window.innerWidth < 720;
        return (
            <div
                style={{
                    marginTop: matches ? '170px' : '0px',
                    display: matches ? 'initial' : 'flex',
                    justifyContent: 'center'
                }}
            >
                <Form onSubmit={this.onSubmit}>
                    <form
                        style={{
                            ...styles.form,
                            padding: matches ? '15px ' : '0px',
                            display: matches ? 'initial' : 'flex',
                            flexDirection: matches ? 'row' : 'column',
                            justifyContent: 'center'
                        }}
                        id="updateForm"
                    >
                        <div
                            style={{
                                marginTop: 0,
                                display: 'flex',
                                flexDirection: matches ? 'column' : 'row',
                                width: matches ? '300px' : '800px',
                                justifyContent: 'space-around',
                                alignItems: 'center'
                            }}
                        >
                            <div
                                style={{
                                    width: matches ? '250px' : '300px'
                                }}
                            >
                                <FormGroup
                                    controlId="firstNameGroup"
                                    validationState={
                                        firstNameError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {firstNameError
                                            ? `${
                                                  intl.messages.firstName
                                              }: ${firstNameError}`
                                            : intl.messages.firstName + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={firstName}
                                        onChange={e =>
                                            this.onChange(
                                                'firstName',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="lastNameGroup"
                                    validationState={
                                        lastNameError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {lastNameError
                                            ? `${
                                                  intl.messages.lastName
                                              }: ${lastNameError}`
                                            : intl.messages.lastName + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        value={lastName}
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'lastName',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="displayName"
                                    validationState={
                                        displayNameError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {displayNameError
                                            ? `${
                                                  intl.messages.displayName
                                              }: ${displayNameError}`
                                            : intl.messages.displayName + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        value={displayName}
                                        type="text"
                                        onChange={e =>
                                            this.onChange(
                                                'displayName',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>

                                <FormGroup
                                    controlId="phoneGroup"
                                    validationState={null}
                                >
                                    <ControlLabel>
                                        {intl.messages.phone}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={phone}
                                        onChange={e =>
                                            this.onChange(
                                                'phone',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                            </div>
                            <div
                                style={{
                                    width: matches ? '250px' : '300px'
                                }}
                            >
                                <p
                                    style={{
                                        fontWeight: 'bold',
                                        color: countryIdError
                                            ? 'rgb(169, 68, 66)'
                                            : 'black',
                                        marginBottom: '5px'
                                    }}
                                >
                                    {countryIdError
                                        ? `${countryIdError}`
                                        : intl.messages.country + ' *'}
                                </p>
                                <Select
                                    name="countries"
                                    value={countryId}
                                    onChange={this.onSelectCountry}
                                    options={countries}
                                    onClose={() => null}
                                    style={{ marginBottom: '15px' }}
                                />
                                <FormGroup
                                    controlId="cityGroup"
                                    validationState={cityError ? 'error' : null}
                                >
                                    <ControlLabel>
                                        {cityError
                                            ? `${
                                                  intl.messages.city
                                              }: ${cityError}`
                                            : intl.messages.city + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={city}
                                        onChange={e =>
                                            this.onChange(
                                                'city',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="addressGroup"
                                    validationState={null}
                                >
                                    <ControlLabel>
                                        {intl.messages.address}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={address}
                                        onChange={e =>
                                            this.onChange(
                                                'address',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                                <FormGroup
                                    controlId="zipCodeGroup"
                                    validationState={
                                        zipCodeError ? 'error' : null
                                    }
                                >
                                    <ControlLabel>
                                        {zipCodeError
                                            ? `${
                                                  intl.messages.zipCode
                                              }: ${zipCodeError}`
                                            : intl.messages.zipCode + ' *'}
                                    </ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={zipCode}
                                        onChange={e =>
                                            this.onChange(
                                                'zipCode',
                                                e.target.value
                                            )
                                        }
                                    />
                                    <FormControl.Feedback />
                                </FormGroup>
                            </div>
                        </div>
                        <div style={styles.formRow}>
                            <p
                                style={{
                                    fontWeight: 'bold',
                                    alignSelf: 'baseline',
                                    marginLeft: matches ? '25px' : 0
                                }}
                            >
                                {intl.messages.language}
                            </p>
                            <Select
                                name="languages"
                                value={languageId}
                                onChange={this.onLanguageChange}
                                options={this.getLanguages()}
                                style={{
                                    marginBottom: '16px',
                                    width: matches ? '250px' : '300px'
                                }}
                            />
                            <Button
                                bsSize="lg"
                                bsStyle="primary"
                                onClick={this.onSubmit}
                                type="button"
                                style={{
                                    width: matches ? '250px' : '300px',
                                    backgroundColor: Colors.appColor
                                }}
                            >
                                {intl.messages.updateProfileCTA}
                            </Button>
                            <Button
                                style={{
                                    width: matches ? '250px' : '300px',
                                    marginTop: '1em',
                                    backgroundColor: Colors.appSecondaryColor
                                }}
                                onClick={this.redirectToPasswordForm}
                            >
                                {intl.messages.changePasswordCTA}
                            </Button>
                        </div>
                    </form>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
        userForm: state.userForm
    };
};

const mapDispatchToProps = dispatch => {
    return {
        signInRequest: payload => dispatch(UserActions.signInRequest(payload)),
        resetUserFlags: () => dispatch(UserActions.resetUserFlags()),
        setUserLanguage: language => dispatch(UserActions.setLanguage(language))
    };
};

const styles = {
    form: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        display: 'flex',
        marginTop: '20px'
    },
    formRow: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        display: 'flex',
        marginBottom: '1em'
    },
    formLabel: {
        display: 'flex',
        alignSelf: 'flex-start',
        marginLeft: '1.9em',
        marginBottom: '.3em'
    },
    checkboxWrapper: {
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    checkbox: {
        fontSize: '1.1em',
        fontWeight: 'bold'
    },
    inputField: {
        padding: '.5em .5em'
    },
    formError: {
        color: '#cc0000'
    },
    error: {
        backgroundColor: '#cc0000',
        color: 'white',
        paddingTop: '.5em',
        paddingBottom: '.5em'
    },
    button: {
        alignSelf: 'center'
    }
};

export default injectIntl(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(UserForm)
);
