export const validateEmail = value => ({
    error: !value ? 'Invalid email' : null,
    success: null,
});
export const validatePassword = value => ({
    error: !value ? 'Invalid password' : null,
    success: null,
});
