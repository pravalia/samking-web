import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
    openSpinner: null,
    closeSpinner: null,
    getAdsSuccess: ['ads'],
    getAdsRequest: null,
    setUserCoords: ['coords']
});

const INITIAL_STATE = Immutable({
    spinnerOpen: false,
    ads: [],
    coords: {
        lat: 0,
        lng: 0
    }
});

const openSpinner = (state = INITIAL_STATE) => state.set('spinnerOpen', true);

const closeSpinner = (state = INITIAL_STATE) => state.set('spinnerOpen', false);

const getAdsSuccess = (state, { ads }) => state.merge({ ads });

const setUserCoords = (state, { coords }) => {
    return state.merge({ coords });
};

export default Creators;
export const AppTypes = Types;

export const reducer = createReducer(INITIAL_STATE, {
    [Types.OPEN_SPINNER]: openSpinner,
    [Types.CLOSE_SPINNER]: closeSpinner,
    [Types.GET_ADS_SUCCESS]: getAdsSuccess,
    [Types.SET_USER_COORDS]: setUserCoords
});
