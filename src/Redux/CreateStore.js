import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer, createTransform } from 'redux-persist';
import hardSet from 'redux-persist/lib/stateReconciler/hardSet';
import Immutable from 'seamless-immutable';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

export default (rootReducer, rootSaga) => {
    const middleware = [];
    const enhancers = [];
    const whitelist = ['user', 'app'];
    const sagaMiddleware = createSagaMiddleware();
    middleware.push(sagaMiddleware);
    enhancers.push(applyMiddleware(...middleware));
    const immutableTransform = createTransform(
        state => {
            return Immutable.isImmutable(state)
                ? state.asMutable({ deep: true })
                : Immutable(state);
        },
        state => {
            return Immutable(state);
        },
        { whitelist },
    );

    const persistConfig: PersistConfig = {
        key: 'root',
        storage,
        debug: true,
        stateReconciler: hardSet,
        transforms: [immutableTransform],
        version: 0,
        whitelist,
    };

    const persistedReducer = persistReducer(persistConfig, rootReducer);
    const store = createStore(
        persistedReducer,
        undefined,
        composeWithDevTools(...enhancers),
    );

    sagaMiddleware.run(rootSaga);

    const persistor = persistStore(store, {}, () =>
        store.dispatch({ type: 'REHYDRATION_COMPLETE' }),
    );
    return { store, persistor };
};
