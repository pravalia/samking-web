import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import { uniq } from 'ramda';

const { Types, Creators } = createActions({
    getHistoryRequest: ['id'],
    getHistoryTotalSuccess: ['total'],
    getHistoryTotalFailure: ['errorMessage'],
    getHistoryAlertsSuccess: ['alerts'],
    getHistoryAlertsFailure: ['errorMessage'],
    getHistorySuccess: ['payload'],
    getHistoryFailure: ['errorMessage'],
    sendPointsRequest: ['payload'],
    sendPointsSuccess: null,
    sendPointsFailure: ['errorMessage'],
    checkAvailableOffers: null,
    hasAvailableOffer: ['availableOffers'],
    removeAvailableOffer: ['id'],
    resetHistoryFlags: null,
    resetHistory: null,
    signOut: null
});

const INITIAL_STATE = Immutable({
    history: null,
    total: null,
    alerts: null,
    offset: 0,
    totalOffset: 0,
    alertsOffset: 0,
    success: false,
    sendSuccess: false,
    errorMessage: '',
    totalErrorMessage: '',
    sendErrorMessage: '',
    alertsErrorMessage: '',
    fetching: false,
    availableOffer: null,
    offersBuffer: []
});

const request = (state = INITIAL_STATE) =>
    state.merge({
        success: false,
        sendSuccess: false,
        sendErrorMessage: '',
        errorMessage: '',
        totalErrorMessage: '',
        fetching: true
    });

const success = (state = INITIAL_STATE, { payload }) =>
    state.merge({
        history: payload.history,
        ranking: payload.ranking,
        offset: state.offset + payload.history.length,
        errorMessage: '',
        success: true,
        fetching: false
    });

const totalSuccess = (state = INITIAL_STATE, { total }) => {
    const result = state.merge({
        total: total,
        totalOffset: state.totalOffset + total.length,
        success: true,
        fetching: false,
        totalErrorMessage: ''
    });
    return result;
};

const alertsSuccess = (state = INITIAL_STATE, { alerts }) =>
    state.merge({
        alerts: alerts,
        alertsOffset: state.alertsOffset + alerts.length,
        success: true,
        fetching: false,
        alertsErrorMessage: ''
    });

const sendSuccess = state =>
    state.merge({
        fetching: false,
        sendSuccess: true
    });

const sendFailure = (state, { errorMessage }) =>
    state.merge({
        fetching: false,
        sendErrorMessage: errorMessage
    });

const failure = (state = INITIAL_STATE, { errorMessage }) =>
    state.merge({ errorMessage, fetching: false });

const totalFailure = (state = INITIAL_STATE, { errorMessage }) =>
    state.merge({ totalErrorMessage: errorMessage, fetching: false });

const alertsFailure = (state = INITIAL_STATE, { errorMessage }) =>
    state.merge({ alertsErrorMessage: errorMessage, fetching: false });

const resetHistoryFlags = state =>
    state.merge({
        success: false,
        errorMessage: '',
        totalErrorMessage: '',
        sendErrorMessage: '',
        fetching: false,
        sendSuccess: false
    });

const hasAvailableOffer = (state, { availableOffers }) => {
    if (
        availableOffers &&
        Array.isArray(availableOffers) &&
        availableOffers.length > 0
    ) {
        const mutableAvailableOffers = [...availableOffers];
        const availableOffer = mutableAvailableOffers.pop();
        return state.merge({
            availableOffer,
            offersBuffer: mutableAvailableOffers
        });
    }
    return state;
};

export const removeAvailableOffer = state => {
    const { offersBuffer } = state;
    if (!Array.isArray(offersBuffer) || offersBuffer.length < 1) {
        return state.set('availableOffer', null);
    }
    const mutableAvailableOffers = [...offersBuffer];
    const availableOffer = mutableAvailableOffers.pop();
    return state.merge({
        availableOffer,
        offersBuffer: mutableAvailableOffers
    });
};

export const reset = state => {
    return INITIAL_STATE;
};

export default Creators;
export const HistoryTypes = Types;

export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_HISTORY_REQUEST]: request,
    [Types.GET_HISTORY_SUCCESS]: success,
    [Types.GET_HISTORY_FAILURE]: failure,
    [Types.GET_HISTORY_TOTAL_SUCCESS]: totalSuccess,
    [Types.GET_HISTORY_TOTAL_FAILURE]: totalFailure,
    [Types.GET_HISTORY_ALERTS_SUCCESS]: alertsSuccess,
    [Types.GET_HISTORY_ALERTS_FAILURE]: alertsFailure,
    [Types.SEND_POINTS_REQUEST]: resetHistoryFlags,
    [Types.SEND_POINTS_SUCCESS]: sendSuccess,
    [Types.SEND_POINTS_FAILURE]: sendFailure,
    [Types.RESET_HISTORY_FLAGS]: resetHistoryFlags,
    [Types.HAS_AVAILABLE_OFFER]: hasAvailableOffer,
    [Types.REMOVE_AVAILABLE_OFFER]: removeAvailableOffer,
    [Types.RESET_HISTORY]: reset,
    [Types.SIGN_OUT]: reset
});
