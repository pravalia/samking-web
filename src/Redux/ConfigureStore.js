import { resettableReducer } from 'reduxsauce';
import { combineReducers } from 'redux';
import createStore from './CreateStore';
import rootSaga from '../Saga/RootSaga';
import { reducer as userReducer } from './User/UserRedux';
import { reducer as userFormReducer } from './User/UserFormRedux';
import { reducer as passwordFormReducer } from './User/PasswordRedux';
import { reducer as merchantsReducer } from './Merchants/MerchantsRedux';
import { reducer as historyReducer } from './History/HistoryRedux';
import { reducer as appReducer } from './AppRedux';

export default () => {
    const resettable = resettableReducer('RESET');
    const reducer = combineReducers({
        user: resettable(userReducer),
        userForm: resettable(userFormReducer),
        passwordForm: resettable(passwordFormReducer),
        product: resettable(merchantsReducer),
        app: resettable(appReducer),
        merchants: resettable(merchantsReducer),
        history: resettable(historyReducer),
    });
    return createStore(reducer, rootSaga);
};
