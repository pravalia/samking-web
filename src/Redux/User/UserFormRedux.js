import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import * as R from 'ramda';
import fieldValidation from './UserFieldValidation';
import { validateForm } from '../../Utils/ValidationUtils';

const { Types, Creators } = createActions({
    updateUserRequest: ['payload'],
    updateUserSuccess: ['user'],
    updateUserFailure: ['errorMessage'],
    resetUserForm: null,
    onUserFormFieldChange: ['field', 'value'],
    initializeForm: ['user'],
    sendResetEmailRequest: ['emailAddress'],
});

const INITIAL_STATE = Immutable({
    fields: {
        firstName: '',
        lastName: '',
        phone: '',
        country: '',
        city: '',
        address: '',
        zipCode: '',
        firstNameError: '',
        lastNameError: '',
        phoneError: '',
    },
    isValid: false,
    success: false,
    fetching: false,
    errorMessage: '',
});

export const request = state => {
    return state.merge({
        fetching: true,
        success: false,
        errorMessage: '',
    });
};

export const updateUserSuccess = state =>
    state.merge({
        success: true,
        fetching: false,
        errorMessage: '',
    });

export const failure = (state = INITIAL_STATE, { errorMessage }) =>
    state.merge({ fetching: false, errorMessage });

export const onUserFormFieldChange = (state, { field, value }) => {
    const validatedState = validateForm({
        state,
        payload: { field, value },
        fieldValidation,
        validatedFields: Object.keys(state.fields),
    });
    return validatedState;
};

export const initializeForm = (state, { user }) => {
    const newFields = R.pick(
        [
            'firstName',
            'lastName',
            'phone',
            'countryId',
            'city',
            'address',
            'zipCode',
        ],
        user,
    );
    const nextFields = state.fields.merge(newFields);
    return state.merge({ fields: nextFields, isValid: true });
};

export const resetUserForm = () => INITIAL_STATE;

export default Creators;
export const UserFormTypes = Types;

export const reducer = createReducer(INITIAL_STATE, {
    [Types.UPDATE_USER_REQUEST]: request,
    [Types.UPDATE_USER_SUCCESS]: updateUserSuccess,
    [Types.UPDATE_USER_FAILURE]: failure,
    [Types.RESET_USER_FORM]: resetUserForm,
    [Types.INITIALIZE_FORM]: initializeForm,
    [Types.ON_USER_FORM_FIELD_CHANGE]: onUserFormFieldChange,
    [Types.SEND_RESET_EMAIL_REQUEST]: request,
});
