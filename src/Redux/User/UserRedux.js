import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import moment from 'moment';

const { Types, Creators } = createActions({
    signInRequest: ['payload'],
    signInSuccess: ['user'],
    signInFailure: ['errorMessage'],
    signOut: null,
    setLanguage: ['language'],
    updateUserSuccess: ['user'],
    resetUserFlags: null,
    resetUser: null,
    signUpRequest: ['payload'],
    getCountriesRequest: null,
    getCountriesSuccess: ['countries'],
    getLanguagesSuccess: ['languages'],
    getLanguagesRequest: null,
});

const INITIAL_STATE = Immutable({
    id: 0,
    userTypeId: 0,
    firstName: '',
    lastName: '',
    phone: '',
    locationId: 0,
    displayName: '',
    companyId: 0,
    token: '',
    languageId: 1,
    countryId: '',
    city: '',
    address: '',
    zipCode: '',
    countries: [],
    languages: [],
    success: false,
    fetching: false,
    errorMessage: '',
    keepSignedIn: false,
});

export const request = (state = INITIAL_STATE) =>
    state.merge({
        fetching: true,
        success: false,
        errorMessage: '',
    });

export const failure = (state = INITIAL_STATE, { errorMessage }) => state.merge({ fetching: false, errorMessage });

export const signInSuccess = (state, { user }) => {
    const { languages } = state;
    const selectedLanguage = languages.find(({ id, name }) => Number(id) === Number(user.languageId || 2));
    return state.merge({
        ...user,
        signInTimestamp: moment().format('DD/MM/YYYY HH:mm'),
        success: true,
        fetching: false,
        language: selectedLanguage.name,
    });
};

export const setLanguage = (state, { language }) => {
    const selectedLanguage = state.languages && state.languages.find(({ id, name }) => name === language);
    return state.merge({
        language,
        languageId: selectedLanguage && selectedLanguage.id,
    });
};

export const signOut = state => {
    return INITIAL_STATE.merge({
        languageId: state.languageId,
        countries: state.countries,
        languages: state.languages,
    });
};

export const resetFlags = (state = INITIAL_STATE) =>
    state.merge({
        fetching: false,
        success: false,
        errorMessage: '',
    });

export const resetUser = state =>
    INITIAL_STATE.merge({
        languages: state.languages,
        language: state.language,
        countries: state.countries,
    });

export const getCountriesSuccess = (state, { countries }) => {
    return state.merge({
        countries,
    });
};

export const getLanguagesSuccess = (state, { languages }) => {
    return state.merge({
        languages,
    });
};

export default Creators;
export const UserTypes = Types;

export const reducer = createReducer(INITIAL_STATE, {
    [Types.SIGN_IN_REQUEST]: request,
    [Types.SIGN_IN_SUCCESS]: signInSuccess,
    [Types.SIGN_IN_FAILURE]: failure,
    [Types.SIGN_OUT]: signOut,
    [Types.SET_LANGUAGE]: setLanguage,
    [Types.UPDATE_USER_SUCCESS]: signInSuccess,
    [Types.RESET_USER_FLAGS]: resetFlags,
    [Types.RESET_USER]: resetUser,
    [Types.GET_COUNTRIES_SUCCESS]: getCountriesSuccess,
    [Types.GET_LANGUAGES_SUCCESS]: getLanguagesSuccess,
    RESET: resetUser,
});
