import validate from 'validate.js';

const firstNameConstraints = {
    firstName: {
        presence: {
            allowEmpty: false,
            message: "^First Name can't be black",
        },
    },
};

const lastNameConstraints = {
    lastName: {
        presence: {
            allowEmpty: false,
            message: "^Last Name can't be black",
        },
    },
};

const emailConstraints = {
    email: {
        presence: {
            allowEmpty: false,
            message: '^Email is invalid',
        },
    },
};

const oldPasswordConstraints = {
    oldPassword: {
        presence: {
            allowEmpty: false,
            message: "^old password can't be blank",
        },
    },
};
const newPasswordConstraints = {
    newPassword: {
        presence: {
            allowEmpty: false,
            message: "^old password can't be blank",
        },
    },
};
const repeatPasswordConstraints = {
    repeatPassword: {
        presence: {
            allowEmpty: false,
            message: "^old password can't be blank",
        },
    },
};

const phonePattern = /^\d+$/;
const phoneConstraints = {
    phone: {
        presence: {
            allowEmpty: false,
            message: "Phone number can't be blank",
        },
        format: {
            pattern: phonePattern,
            flags: 'i',
            message: 'Invalid phone number',
        },
    },
};

export default function fieldValidation(state, payload) {
    const { field, value } = payload;
    switch (field) {
        case 'firstName':
            const validFirstName = validate(
                {
                    firstName: value,
                },
                firstNameConstraints,
            );
            return state.setIn(
                ['fields', 'firstNameError'],
                validFirstName !== undefined ? validFirstName.firstName[0] : '',
            );

        case 'lastName':
            const validLastName = validate(
                {
                    lastName: value,
                },
                lastNameConstraints,
            );

            return state.setIn(
                ['fields', 'lastNameError'],
                validLastName !== undefined ? validLastName.lastName[0] : '',
            );
        case 'email':
            const validEmail = validate(
                {
                    email: value,
                },
                emailConstraints,
            );

            return state.setIn(
                ['fields', 'emailError'],
                validEmail !== undefined ? validEmail.email[0] : '',
            );
        case 'oldPassword':
            const validOldPassword = validate(
                {
                    oldPassword: value,
                },
                oldPasswordConstraints,
            );

            return state.setIn(
                ['fields', 'oldPasswordError'],
                validOldPassword !== undefined
                    ? validOldPassword.oldPassword[0]
                    : '',
            );
        case 'newPassword':
            const validNewPassword = validate(
                {
                    newPassword: value,
                },
                newPasswordConstraints,
            );

            return state.setIn(
                ['fields', 'newPasswordError'],
                validNewPassword !== undefined
                    ? validNewPassword.newPassword[0]
                    : '',
            );
        case 'repeatPassword':
            const validRepeatPassword = validate(
                {
                    repeatPassword: value,
                },
                repeatPasswordConstraints,
            );

            return state.setIn(
                ['fields', 'repeatPasswordError'],
                validRepeatPassword !== undefined
                    ? validRepeatPassword.repeatPassword[0]
                    : '',
            );

        case 'phone':
            const validphone = validate(
                {
                    phone: value,
                },
                phoneConstraints,
            );

            return state.setIn(
                ['fields', 'phoneError'],
                validphone !== undefined ? validphone.phone[0] : '',
            );

        default:
            return state;
    }
}
