import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';
import fieldValidation from './UserFieldValidation';
import { validateForm } from '../../Utils/ValidationUtils';

const { Types, Creators } = createActions({
    updatePasswordRequest: ['payload'],
    updatePasswordSuccess: null,
    updatePasswordFailure: ['errorMessage'],
    resetPasswordForm: null,
    resetPasswordFormFlags: null,
    onPasswordFormFieldChange: ['field', 'value'],
    checkForEmptyFields: ['intl'],
    resetPasswordRequest: ['payload'],
});

const INITIAL_STATE = Immutable({
    fields: {
        oldPassword: '',
        newPassword: '',
        repeatPassword: '',
        oldPasswordError: '',
        newPasswordError: '',
        repeatPasswordError: '',
    },
    isValid: false,
    success: false,
    fetching: false,
    errorMessage: '',
});

export const request = state =>
    state.merge({
        fetching: true,
        success: false,
        errorMessage: '',
    });

export const success = state =>
    state.merge({
        success: true,
        fetching: false,
        errorMessage: '',
    });
export const failure = (state = INITIAL_STATE, { errorMessage }) => state.merge({ fetching: false, errorMessage });

export const onPasswordFormFieldChange = (state, { field, value }) => {
    return validateForm({
        state,
        payload: { field, value },
        fieldValidation,
        validatedFields: Object.keys(state.fields),
    });
};

export const checkForEmptyFields = (state, { intl }) => {
    const missedErrors = {};
    for (const field of Object.keys(state.fields)) {
        if (!state.fields[field] && !field.includes('Error') && !state.fields[`${field}Error`]) {
            missedErrors[`${field}Error`] = intl.messages.cannotBeBlank;
        }
    }
    const nextFields = state.fields.merge(missedErrors);
    return state.set('fields', nextFields);
};

export const resetPasswordForm = () => INITIAL_STATE;

export const resetPasswordFormFlags = state =>
    INITIAL_STATE.merge({
        fields: state.fields,
        isValid: state.isValid,
    });

export default Creators;
export const PasswordTypes = Types;

export const reducer = createReducer(INITIAL_STATE, {
    [Types.UPDATE_PASSWORD_REQUEST]: request,
    [Types.UPDATE_PASSWORD_SUCCESS]: success,
    [Types.UPDATE_PASSWORD_FAILURE]: failure,
    [Types.RESET_PASSWORD_FORM]: resetPasswordForm,
    [Types.RESET_PASSWORD_FORM_FLAGS]: resetPasswordFormFlags,
    [Types.ON_PASSWORD_FORM_FIELD_CHANGE]: onPasswordFormFieldChange,
    [Types.CHECK_FOR_EMPTY_FIELDS]: checkForEmptyFields,
    [Types.RESET_PASSWORD_REQUEST]: request,
});
