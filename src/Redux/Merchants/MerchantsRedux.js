import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

const { Types, Creators } = createActions({
    getMerchantsRequest: ['lat', 'lng', 'zoom'],
    getMerchantsSuccess: ['merchants'],
    getMerchantsFailure: ['errorMessage'],
});

const INITIAL_STATE = Immutable({
    merchants: [],
    success: false,
    errorMessage: '',
    fetching: false,
});

const request = (state = INITIAL_STATE) =>
    state.merge({
        success: false,
        errorMessage: '',
        fetching: false,
    });

const success = (state = INITIAL_STATE, { merchants }) =>
    INITIAL_STATE.merge({ merchants });

const failure = (state = INITIAL_STATE, { errorMessage }) =>
    INITIAL_STATE.merge({ errorMessage });

export default Creators;
export const MerchantsTypes = Types;

export const reducer = createReducer(INITIAL_STATE, {
    [Types.GET_MERCHANTS_REQUEST]: request,
    [Types.GET_MERCHANTS_SUCCESS]: success,
    [Types.GET_MERCHANTS_FAILURE]: failure,
});
