const fontFamily = 'Verdana';

const size = {
    title: 18,
    headerTitle: 24,
    cardNumber: 18,
    medium: 16,
    regular: 14,
    small: 12,
    status: 11,
    tiny: 10,
};

const style = {
    normal: {
        fontFamily,
        fontSize: size.regular,
    },
    title: {
        fontFamily,
        fontSize: size.title,
        fontWeight: '500',
    },
    headerTitle: {
        fontFamily,
        fontSize: size.headerTitle,
    },
    small: {
        fontFamily,
        fontSize: size.small,
    },
    medium: {
        fontFamily,
        fontSize: size.medium,
    },
    tiny: {
        fontFamily,
        fontSize: size.tiny,
    },
};

export default {
    size,
    style,
};
