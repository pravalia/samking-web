import Moment from 'moment';

export const getInternationalizedErrorMessage = (errorMessage, I18n) => {
    const possibleMessage = I18n.messages.apiErrors[errorMessage];
    if (!possibleMessage) {
        return I18n.messages.apiErrors['server_error'];
    }
    return possibleMessage;
};

export const sortBy = (sortCriteria, group) => {
    return group.sort((item1, item2) => {
        const attribute1 = item1[sortCriteria];
        const attribute2 = item2[sortCriteria];
        const isDate = typeof attribute1 === 'string' && Moment(attribute1, 'YYYY-MM-DD HH:mm').isValid();
        if (isDate) {
            return Moment(attribute2).diff(Moment(attribute1));
        }
        const isNumber = !isNaN(Number(attribute1));

        if (isNumber) {
            return attribute2 - attribute1;
        }
        return Number(String(attribute1) > String(attribute2));
    });
};
