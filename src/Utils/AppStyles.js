import Fonts from './Fonts';
import Metrics from './Metrics';
import Colors from './Colors';

const AppStyles = {
    controls: {
        appView: {
            display: 'flex',
            flex: 1,
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center',
            fontFamily: 'Verdana',
        },
        headerTitle: {
            marginBottom: `${Metrics.baseMargin / 2}px`,
            color: Colors.title,
            backgroundColor: 'transparent',
            ...Fonts.style.title,
        },
        generalText: {
            marginBottom: `${Metrics.baseMargin / 2}px`,
            color: Colors.text,
            backgroundColor: 'transparent',
            ...Fonts.style.normal,
        },
        generalTextLight: {
            marginBottom: `${Metrics.baseMargin / 2}px`,
            color: Colors.textLight,
            backgroundColor: 'transparent',
            ...Fonts.style.small,
        },
        errorText: {
            textAlign: 'center',
            width: window.innerWidth - Metrics.baseMargin * 4,
            color: Colors.error,
            backgroundColor: 'transparent',
            ...Fonts.style.medium,
            alignSelf: 'center',
        },
        contextStackingLevelThree: {
            zIndex: 3,
            elevation: 3,
        },
    },
    screen: {
        appView: {
            display: 'flex',
            flex: 1,
            width: window.innerWidth,
            backgroundColor: Colors.appViewBackground,
        },
        innerView: {
            display: 'flex',
            flex: 1,
            width: window.innerWidth,
            marginHorizontal: Metrics.baseMargin,
            alignItems: 'center',
            justifyContent: 'center',
        },
        contentView: {
            flex: 1,
            width: window.innerWidth,
        },
        contentTop: {
            justifyContent: 'flex-start',
        },
        contentCenter: {
            justifyContent: 'center',
            alignItems: 'center',
        },
    },
};

export default AppStyles;
