const colors = {
    text: '#444444',
    textLight: 'white',
    error: '#cc0000',
    appColor: '#103366',
    appSecondaryColor: '#e2b87f'
};

export default colors;
