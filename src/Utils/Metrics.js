const metrics = {
    marginHorizontal: '10px',
    marginVertical: '10px',
    baseMargin: '16px',
    smallMargin: '5px',
    isIE:
        navigator.appName === 'Microsoft Internet Explorer' ||
        (navigator.userAgent.match(/Trident/) ||
            navigator.userAgent.match(/rv:11/)),
    isShitBrowser: false,
    navbarHeight: 48
};

if (!global.Intl) {
    global.Intl = require('intl');
    metrics.isShitBrowser = true;
}

calculateScreenSizes(metrics);

window.onresize = calculateScreenSizes(metrics);

function calculateScreenSizes(metrics) {
    metrics.screenHeight = window.innerHeight;
    metrics.navbarHeight = 48;
}

export default metrics;
