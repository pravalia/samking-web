/**
 * @flow
 */

import * as R from 'ramda';
import _ from 'lodash';

export const toLowerDeburr = value => {
    return _.deburr(value.trim().toLowerCase());
};

export const capitalizeFirstLetter = value => {
    return value.charAt(0).toUpperCase() + value.slice(1);
};

export const areSameValues = (value1, value2) => {
    return toLowerDeburr(String(value1)) === toLowerDeburr(String(value2));
};

const isFormValid = ({ fields, validatedFields }) => {
    let emptyFields = false;
    let errors = false;

    for (const fieldName in fields) {
        if (!validatedFields.includes(fieldName)) {
            continue;
        }

        if (emptyFields || errors) {
            break;
        }

        if (fieldName.indexOf('Error') > -1) {
            errors = Boolean(fields[fieldName]);
        } else {
            emptyFields = !fields[fieldName];
        }
    }
    return !emptyFields && !errors;
};

export const validateForm = ({
    state,
    payload,
    fieldValidation,
    validatedFields,
}) => {
    let nextState = state.set('error', '');
    if (payload && payload.field) {
        const { field, value } = payload;
        nextState = state.setIn(['fields', field], value);
        nextState = fieldValidation(nextState, { field, value });
    } else {
        R.forEachObjIndexed((value, key) => {
            if (!key.includes('Error')) {
                nextState = fieldValidation(nextState, { field: key, value });
            }
        }, nextState.fields);
    }
    const finalState = nextState.setIn(
        ['isValid'],
        isFormValid({ fields: nextState.fields, validatedFields }),
    );
    return finalState;
};

export const hasFieldsErrors = ({
    fields,
    blackList = [],
    whiteList = [],
} = {}) => {
    if (!fields) {
        return false;
    }
    let fieldErrorsCount = 0;
    const hasBlackList = blackList.length > 0;
    const hasWhiteList = whiteList.length > 0;

    if (hasBlackList && hasWhiteList) {
        throw new Error('Pass either blacklist or whiteList, not both');
    }

    for (const key of Object.keys(fields)) {
        if (
            key.includes('Error') &&
            fields[key].length &&
            ((!hasBlackList && !hasWhiteList) ||
                (hasBlackList && blackList.includes(key)) ||
                (hasWhiteList && !whiteList.includes(key)))
        ) {
            fieldErrorsCount++;
        }
    }
    return fieldErrorsCount > 0;
};
