import { put, select } from 'redux-saga/effects';
import AppActions from '../Redux/AppRedux';

export function* getAdsRequest(api) {
    const state = yield select();
    const { coords } = state.app;
    const { lat, lng } = coords;
    const response = yield api.getAds(lat, lng);
    if (response.ok) {
        yield put(AppActions.getAdsSuccess(response.data));
    }
}
