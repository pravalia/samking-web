import { put, select } from 'redux-saga/effects';
import { sortBy } from '../Utils/CommonFunctions';
import HistoryActions from '../Redux/History/HistoryRedux';

export function* getHistoryRequest(api, { id }) {
    const state = yield select();
    const { user, history } = state;
    const { offset, totalOffset, alertsOffset } = history;
    const userId = id ? id : user.id;
    const response = yield api.getHistory(userId, offset);

    const responseRanking = yield api.getRanking();
    if (response.ok && responseRanking.ok) {
        yield put(
            HistoryActions.getHistorySuccess({
                history: response.data,
                ranking: responseRanking.data
            })
        );
    } else {
        yield put(
            HistoryActions.getHistoryFailure(
                response.data.errorMessage || responseRanking.data.errorMessage
            )
        );
    }
    const totalResponse = yield api.getHistoryTotal(
        userId,
        0,
        false,
        user.language
    );
    const alertsResponse = yield api.getHistoryTotal(
        userId,
        0,
        true,
        user.language
    );
    if (totalResponse.ok) {
        yield put(HistoryActions.getHistoryTotalSuccess(totalResponse.data));
    } else {
        yield put(
            HistoryActions.getHistoryTotalFailure(
                totalResponse.data.errorMessage
            )
        );
    }
    if (alertsResponse.ok) {
        yield put(HistoryActions.getHistoryAlertsSuccess(alertsResponse.data));
    } else {
        yield put(
            HistoryActions.getHistoryAlertsFailure(
                alertsResponse.data.errorMessage
            )
        );
    }
}

export function* checkAvailableOffers(api) {
    const response = yield api.checkAvailableOffers();

    if (response.ok) {
        yield put(HistoryActions.hasAvailableOffer(response.data));
    } else {
        //ignore
    }
}

export function* removeAvailableOffer(api, { id }) {
    yield api.removeAvailableOffer(id);
}

export function* sendPointsRequest(api, { payload }) {
    const response = yield api.sendPoints(payload);
    if (response.ok) {
        yield put(HistoryActions.sendPointsSuccess());
        yield put(HistoryActions.getHistoryRequest());
    } else {
        yield put(HistoryActions.sendPointsFailure(response.data.errorMessage));
    }
}
