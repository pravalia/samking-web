import { put } from 'redux-saga/effects';
import MerchantsActions from '../Redux/Merchants/MerchantsRedux';
import AppActions from '../Redux/AppRedux';

export function* getMerchantsRequest(api, { lat, lng, zoom }) {
    let radius = (40000 / Math.pow(2, zoom)) * 2;
    if (radius < 10) {
        radius = 10;
    }
    const response = yield api.getMerchants(lat, lng, radius);
    if (response.ok) {
        yield put(MerchantsActions.getMerchantsSuccess(response.data));
    } else {
        yield put(
            MerchantsActions.getMerchantsFailure(response.data.errorMessage)
        );
    }
    yield put(AppActions.getAdsRequest());
}
