import { put, select } from 'redux-saga/effects';
import * as R from 'ramda';
import moment from 'moment';
import UserActions from '../Redux/User/UserRedux';
import HistoryActions from '../Redux/History/HistoryRedux';
import UserFormActions from '../Redux/User/UserFormRedux';
import PasswordActions from '../Redux/User/PasswordRedux';
import AppActions from '../Redux/AppRedux';

export function* signInRequest(api, { payload }) {
    payload.signUp = false;
    const response = yield api.signIn(payload);
    if (response.ok) {
        api.setHeader('Authorization', response.data.token.token);
        yield put(HistoryActions.resetHistory());
        yield put(HistoryActions.getHistoryRequest(response.data.id));
        yield put(HistoryActions.checkAvailableOffers());
        yield put(UserActions.getCountriesRequest(api));
        yield put(UserActions.getLanguagesRequest(api));
        yield put(AppActions.getAdsRequest(api));

        yield put(
            UserActions.signInSuccess({
                ...response.data,
                languageId: response.data.languageId || 2,
                keepSignedIn: payload.keepSignedIn
            })
        );
    } else {
        yield put(UserActions.signInFailure(response.data.errorMessage));
    }
}

export function* signUpRequest(api, { payload }) {
    payload.signUp = true;
    const keepSignedIn = true;
    const response = yield api.signUp(payload);
    if (response.ok) {
        api.setHeader('Authorization', response.data.token.token);
        yield put(HistoryActions.resetHistory());
        yield put(HistoryActions.getHistoryRequest(response.data.id));
        yield put(HistoryActions.checkAvailableOffers());
        yield put(UserActions.getCountriesRequest(api));
        yield put(AppActions.getAdsRequest(api));
        yield put(UserActions.getLanguagesRequest(api));

        yield put(
            UserActions.signInSuccess({
                ...response.data,
                languageId: response.data.languageId || 2,
                keepSignedIn
            })
        );
    } else {
        yield put(UserActions.signInFailure(response.data.errorMessage));
    }
}

export function* sendResetEmailRequest(api, { emailAddress }) {
    const response = yield api.sendResetEmail(emailAddress);
    if (response.ok) {
        yield put(PasswordActions.updatePasswordSuccess());
    } else {
        yield put(
            PasswordActions.updatePasswordFailure(response.data.errorMessage)
        );
    }
}

export function* resetPasswordRequest(api, { payload }) {
    const response = yield api.resetPassword(payload);
    if (response.ok) {
        yield put(PasswordActions.updatePasswordSuccess());
    } else {
        yield put(
            PasswordActions.updatePasswordFailure(response.data.errorMessage)
        );
    }
}

export function* getCountriesRequest(api) {
    const response = yield api.getCountries();
    if (response.ok) {
        yield put(UserActions.getCountriesSuccess(response.data));
    }
}

export function* getLanguagesRequest(api) {
    const defaultLanguages = [
        {
            id: 1,
            name: 'Deutsch'
        },
        { id: 2, name: 'English' }
    ];
    const response = yield api.getLanguages();
    const languages = response.ok ? response.data : defaultLanguages;
    yield put(UserActions.getLanguagesSuccess(languages));
}

export function* updateUserRequest(api, { payload }) {
    const state = yield select();
    payload.id = state.user.id;
    const response = yield api.updateUser(payload);
    if (response.ok) {
        yield put(UserActions.updateUserSuccess(response.data));
    } else {
        yield put(
            UserFormActions.updateUserFailure(response.data.errorMessage)
        );
    }
}

export function* updatePasswordRequest(api) {
    const state = yield select();
    const payload = R.pick(
        ['oldPassword', 'newPassword', 'repeatPassword'],
        state.passwordForm.fields
    );
    payload.id = state.user.id;
    payload.type = 'client';
    const response = yield api.updatePassword(payload);
    if (response.ok) {
        yield put(PasswordActions.updatePasswordSuccess());
    } else {
        yield put(
            PasswordActions.updatePasswordFailure(response.data.errorMessage)
        );
    }
}

export function* checkIfNeedsSignedOut(api) {
    try {
        const state = yield select();
        const { keepSignedIn, token, id } = state.user;
        const now = moment();
        const expiresAt = moment(token.expiresAt, 'DD/MM/YYYY HH:mm');
        const expired = expiresAt.isBefore(now);
        if (!keepSignedIn || expired) {
            return yield put(UserActions.signOut());
        }
        api.setHeader('Authorization', token.token);
        yield put(HistoryActions.getHistoryRequest(id));
        yield put(HistoryActions.checkAvailableOffers());
        yield put(UserActions.getCountriesRequest(api));
        yield put(AppActions.getAdsRequest(api));
    } catch (e) {
        console.log(e);
    }
}
