import { all, call, put, fork, takeLatest, select } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { UserFormTypes } from '../Redux/User/UserFormRedux';
import { UserTypes } from '../Redux/User/UserRedux';
import { HistoryTypes } from '../Redux/History/HistoryRedux';
import { MerchantsTypes } from '../Redux/Merchants/MerchantsRedux';
import { PasswordTypes } from '../Redux/User/PasswordRedux';
import { AppTypes } from '../Redux/AppRedux';
import AppActions from '../Redux/AppRedux';
import UserActions from '../Redux/User/UserRedux';

import {
    signUpRequest,
    signInRequest,
    updateUserRequest,
    updatePasswordRequest,
    checkIfNeedsSignedOut,
    sendResetEmailRequest,
    resetPasswordRequest,
    getCountriesRequest,
    getLanguagesRequest,
} from '../Saga/UserSaga';
import { getMerchantsRequest } from '../Saga/MerchantsSaga';
import {
    getHistoryRequest,
    sendPointsRequest,
    removeAvailableOffer,
    checkAvailableOffers,
} from '../Saga/HistorySaga';

import { getAdsRequest } from '../Saga/AppSaga';

import Api from '../API';

let errorMessages = {};

const defaultApi = Api.create();

const { setHeader, ...rest } = defaultApi;
const api = {
    setHeader,
};

Object.keys(rest).forEach(functionName => {
    api[functionName] = (...args) => {
        const newCall = call(defaultApi[functionName], ...args);
        return callApi(newCall, functionName);
    };
});

const requestSpinnerBlacklist = [
    'getHistory',
    'getHistoryTotal',
    'getRanking',
    'getMerchants',
    'checkAvailableOffers',
    'getCountries',
    'removeAvailableOffer',
    'getLanguages',
    'getAds',
];

function* callApi(apiCall, functionName) {
    if (!requestSpinnerBlacklist.includes(functionName)) {
        yield call(delay, 100);
        yield put(AppActions.openSpinner());
    }

    const response = yield apiCall;
    if (!response.ok) {
        if (
            response.status === 403 &&
            response.data.errorMessage === 'session_expired'
        ) {
            yield put(UserActions.signOut());
        }
        response.data.errorMessage =
            errorMessages.apiErrors[response.data.errorMessage] ||
            errorMessages.apiErrors.server_error;
    }
    if (!requestSpinnerBlacklist.includes(requestSpinnerBlacklist)) {
        yield fork(function*() {
            yield call(delay, 50);
            yield put(AppActions.closeSpinner());
        });
    }

    return response;
}

// function* setAuthHeader({ type, value }) {
//     yield api.setHeader('Authorization', value);
// }

function* setErrorMessages() {
    const { user } = yield select();
    const language = user.language || 'English';
    try {
        errorMessages = require(`../Intl/${language}.json`);
    } catch (e) {
        errorMessages = require('../Intl/English.json');
    }
    return errorMessages;
}

function* postRehydrationActions(api) {
    yield setErrorMessages();
    yield checkIfNeedsSignedOut(api);
}

export default function* root() {
    yield all([
        takeLatest(AppTypes.GET_ADS_REQUEST, getAdsRequest, api),
        takeLatest(UserTypes.SIGN_IN_REQUEST, signInRequest, api),
        takeLatest(UserTypes.SIGN_UP_REQUEST, signUpRequest, api),
        takeLatest(
            UserFormTypes.SEND_RESET_EMAIL_REQUEST,
            sendResetEmailRequest,
            api,
        ),
        takeLatest(UserTypes.GET_COUNTRIES_REQUEST, getCountriesRequest, api),
        takeLatest(UserFormTypes.UPDATE_USER_REQUEST, updateUserRequest, api),
        takeLatest(
            PasswordTypes.UPDATE_PASSWORD_REQUEST,
            updatePasswordRequest,
            api,
        ),
        takeLatest(HistoryTypes.GET_HISTORY_REQUEST, getHistoryRequest, api),
        takeLatest(
            MerchantsTypes.GET_MERCHANTS_REQUEST,
            getMerchantsRequest,
            api,
        ),
        takeLatest(
            HistoryTypes.CHECK_AVAILABLE_OFFERS,
            checkAvailableOffers,
            api,
        ),
        takeLatest(
            HistoryTypes.REMOVE_AVAILABLE_OFFER,
            removeAvailableOffer,
            api,
        ),
        takeLatest(
            PasswordTypes.RESET_PASSWORD_REQUEST,
            resetPasswordRequest,
            api,
        ),
        takeLatest(UserTypes.GET_LANGUAGES_REQUEST, getLanguagesRequest, api),
        takeLatest(HistoryTypes.SEND_POINTS_REQUEST, sendPointsRequest, api),
        takeLatest('REHYDRATION_COMPLETE', postRehydrationActions, api),
    ]);
}
