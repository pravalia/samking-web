let numberOfPoints = 0;
const durationInMs = 30000;
const endpoint = 'https://app.samking.at/api/history';
let authHeader = '';
onmessage = function(payload) {
    const points = payload.data.points;
    const apiKey = payload.data.apiKey;
    numberOfPoints = points;
    authHeader = apiKey;
};

setInterval(function() {
    checkPoints();
}, durationInMs);

function checkPoints() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            parseResponse(this.response);
        }
    };
    xhttp.open('GET', endpoint, true);
    xhttp.setRequestHeader('Authorization', authHeader);
    xhttp.send();
}

function parseResponse(response) {
    const parsedResponse = JSON.parse(response);
    const newNumberOfPoints = parsedResponse.NumberOfPoints;
    if (
        newNumberOfPoints != null &&
        numberOfPoints != null &&
        numberOfPoints !== newNumberOfPoints
    ) {
        const received = numberOfPoints < newNumberOfPoints;
        const redeemed = numberOfPoints > newNumberOfPoints;
        const numberDiff = Number(newNumberOfPoints) - Number(numberOfPoints);
        if (received || redeemed) {
            const numberOfNewPoints = Math.abs(numberDiff);
            postMessage({
                received: received,
                numberOfNewPoints: numberOfNewPoints
            });
        }
        numberOfPoints = newNumberOfPoints;
    }
}
